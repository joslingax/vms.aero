@extends('layouts.app')

@section('content')
<div class="login-box ">
  <div class="resetting-logo pl-0 pr-0">
    <a href="javascript:void(0)"><b>Reset Password </b> |  VMS Aero</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
      
    <div class="card-body login-card-body">
      <p class="login-box-msg pl-0 ml-0 " style="text-align:justify ">Enter your new password</p>

      <form method="POST" action="{{ route('password.update') }}">
         @csrf 
         <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group row">
                <div class="col-md-12">
                    <input id="email" placeholder="Enter your e-mail address" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">

                <div class="col-md-12">
                    <input id="password" placeholder="Enter your new password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">

                <div class="col-md-12">
                    <input id="password-confirm" placeholder="Confirm your new password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Reset Password') }}
                    </button>
                </div>
            </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
@endsection


<!DOCTYPE html>
<html class="cspio">
<head>
	<!---
	Welcome to our free coming soon page template by http://comingsoonpage.com Get your free download at https://www.seedprod.com/free-coming-soon-page/
	This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License
	http://creativecommons.org/licenses/by-nc-nd/4.0/

	To get started follow this checklist.

	1. Replace the *Page Title* on lines 23, 31 & 296
	2. Set the *Page Description* on lines 26, 32 & 298
	3. Set an optional *Logo* on lines 294 using the src attribute.
	4. Set your *Facebook Profile* URL on line 312 using the href attribute.
	5. Set your *Twitter Profile* URL on line 313 using the href attribute.
	6. Set your *Email* on line 314 using the href attribute.
	7. Set your background image on line 63, see file README.mb for additional background images.
	8. Set your MailChimp email post URL on line 300 See video: https://youtu.be/YUdP1qfMot8
	9. FTP to your Website. 
	-->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Vms Aero</title>

	<meta name="generator" content="comingsoonpage.com 1.0.0" />
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:url" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Site sous maintenance" />
	<meta property="og:description" content="" />
	
	<!-- Font Awesome CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

	<!-- Bootstrap and default Style -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://static.comingsoonpage.com/cspio-assets/1.0.0/style.css">

	<!-- Google Fonts -->
	<link class="gf-headline" href='https://fonts.googleapis.com/css?family=Pacifico:400&subset=' rel='stylesheet' type='text/css'>
			
	<!-- Animate CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
	
	<!-- Calculated Styles -->
	<style type="text/css">
	
	html {
		height: 100%;
		overflow: hidden;
	}

	body {
		height:100%;
		overflow: auto;
		-webkit-overflow-scrolling: touch;
	}
	
	html{
		height:100%;
		background: #ffffff url('images/wp4128843.png');
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
    background-size: cover;
    background-repeat:no-repeat ;
    background-position: center center;
	}

	#cspio-page{
		background-color: rgba(0,0,0,0);
	}
	
	.flexbox #cspio-page{
		align-items: center;
		justify-content: center;
	}

	.cspio body{
		background: transparent;
	}

	.cspio body, .cspio body p{
        font-family: Helvetica, Arial, sans-serif;
		font-weight: 400;
    font-size: 15px;
        line-height: 1.50em;
        color:#ffffff;
    }

	::-webkit-input-placeholder {
		font-family:Helvetica, Arial, sans-serif;
		font-weight: 400;
		
	}

	::-moz-placeholder {
		font-family:Helvetica, Arial, sans-serif;
		font-weight: 400;
		
	} 

	:-ms-input-placeholder {
		font-family:Helvetica, Arial, sans-serif;
		font-weight: 400;
		
	} 

	:-moz-placeholder {
		font-family:Helvetica, Arial, sans-serif;
		font-weight: 400;
		
	}

    .cspio h1, .cspio h2, .cspio h3, .cspio h4, .cspio h5, .cspio h6{
        font-family: 'Pacifico';
        color:#ffffff;
    }

	#cspio-headline{
		font-family: 'Pacifico';
		font-weight: 400;
		
				font-size: 48px;
		color:#ffffff;
		line-height: 1.00em;
	}

	.cspio button{
        font-family: Helvetica, Arial, sans-serif;
		font-weight: 400;
		
    }
	
    .cspio a, .cspio a:visited, .cspio a:hover, .cspio a:active{
		color: #ffffff;
	}

	#cspio-socialprofiles a {
	  color: #ffffff;
	}
	.cspio .btn-primary,
	.cspio .btn-primary:focus,
	.gform_button,
	#mc-embedded-subscribe, .submit-button {
		color: black;
		text-shadow: 0 -1px 0 rgba(255,255,255,0.3);
		background-color: #ffffff;
		background-image: -moz-linear-gradient(top,#ffffff,#d9d9d9);
		background-image: -ms-linear-gradient(top,#ffffff,#d9d9d9);
		background-image: -webkit-gradient(linear,0 0,0 100%,from(#ffffff),to(#d9d9d9));
		background-image: -webkit-linear-gradient(top,#ffffff,#d9d9d9);
		background-image: -o-linear-gradient(top,#ffffff,#d9d9d9);
		background-image: linear-gradient(top,#ffffff,#d9d9d9);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#d9d9d9', GradientType=0);
		border-color: #d9d9d9 #d9d9d9 #b3b3b3;
		border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
		*background-color: #d9d9d9;
		filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
	}

	.cspio .btn-primary:hover,
	.cspio .btn-primary:active,
	.cspio .btn-primary.active,
	.cspio .btn-primary.disabled,
	.cspio .btn-primary[disabled],
	.cspio .btn-primary:focus:hover,
	.cspio .btn-primary:focus:active,
	.cspio .btn-primary:focus.active,
	.cspio .btn-primary:focus.disabled,
	.cspio .btn-primary:focus[disabled],
	#mc-embedded-subscribe:hover,
	#mc-embedded-subscribe:active,
	#mc-embedded-subscribe.active,
	#mc-embedded-subscribe.disabled,
	#mc-embedded-subscribe[disabled] {
		background-color: #d9d9d9;
		*background-color: #cccccc;
	}

	.cspio .btn-primary:active,
	.cspio .btn-primary.active,
	.cspio .btn-primary:focus:active,
	.cspio .btn-primary:focus.active,
	.gform_button:active,
	.gform_button.active,
	#mc-embedded-subscribe:active,
	#mc-embedded-subscribe.active {
		background-color: #bfbfbf;
	}

	.form-control,
	.progress {
		background-color: rgba(255, 255, 255, 0.85);
	}

	#cspio-progressbar span,
	.countdown_section {
		color: black;
		text-shadow: 0 -1px 0 rgba(255,255,255,0.3);
	}

	.cspio .btn-primary:hover,
	.cspio .btn-primary:active {
		color: black;
		text-shadow: 0 -1px 0 rgba(255,255,255,0.3);
		border-color: #e6e6e6;
	}
  #loader {
  width: 70px;
  height: 70px;
  border-style: solid;
  border-top-color: #FFF;
  border-right-color: #FFF;
  border-left-color: transparent;
  border-bottom-color: transparent;
  border-radius: 50%;
  box-sizing: border-box;
  animation: rotate 3s ease-in-out infinite;
  transform: rotate(-200deg);
  margin:auto;
  left:0;
  right:0;
  top:0;
  bottom:0;
  z-index: 3;
  position:fixed;
}
@keyframes rotate {
  0% { border-width: 10px; }
  25% { border-width: 3px; }
  50% { 
    transform: rotate(115deg); 
    border-width: 10px;
  }
  75% { border-width: 3px;}
  100% { border-width: 10px;}
}

	.cspio input[type='text']:focus {
		webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075), 0 0 8px rgba(217,217,217,0.6);
		-moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075), 0 0 8px rgba(217,217,217,0.6);
		box-shadow: inset 0 1px 1px rgba(0,0,0,0.075), 0 0 8px rgba(217,217,217,0.6);
	}
    
    #cspio-content {
		display: none;
		max-width: 600px;
		background-color: #000000;
		-webkit-border-radius: 2px;
		border-radius: 2px;
		-moz-background-clip: padding;
		-webkit-background-clip: padding-box;
		background-clip: padding-box;
		background-color:transparent;
	}
    
	.cspio .progress-bar,
	.countdown_section,
	.cspio .btn-primary,
	.cspio .btn-primary:focus,
	.gform_button {
		background-image: none;
		text-shadow: none;
	}

	.cspio input,
	.cspio input:focus {
		-webkit-box-shadow: none !important;
		box-shadow: none !important;
	}
							
	#cspio-page{
	    background: -moz-radial-gradient(ellipse at center, rgba(0, 0, 0, 0.3) 0%,rgba(0, 0, 0, 0.2) 37%,rgba(0,0,0,0) 68%,rgba(0,0,0,0) 100%);
	    background: -webkit-radial-gradient(ellipse at center, rgba(0, 0, 0, 0.3) 0%,rgba(0, 0, 0, 0.2) 37%,rgba(0,0,0,0) 68%,rgba(0,0,0,0) 100%);
	    background: radial-gradient(ellipse at center, rgba(0, 0, 0, 0.3) 0%,rgba(0, 0, 0, 0.2) 37%,rgba(0,0,0,0) 68%,rgba(0,0,0,0) 100%);
	}

	.cspio body{
		background: -moz-radial-gradient(center, ellipse cover,  rgba(0,0,0,0) 7%, rgba(0,0,0,0) 80%, rgba(0,0,0,0.23) 100%); 
		background: -webkit-radial-gradient(center, ellipse cover,  rgba(0,0,0,0) 7%,rgba(0,0,0,0) 80%,rgba(0,0,0,0.23) 100%); 
		background: radial-gradient(ellipse at center,  rgba(0,0,0,0) 7%,rgba(0,0,0,0) 80%,rgba(0,0,0,0.23) 100%); 
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#3b000000',GradientType=1 ); 
	}

	#cspio-subscribe-btn{
	    background:transparent;
	    border: 1px solid #fff !important;
	    color: #fff;
	}

	#cspio-subscribe-btn:hover{
	    background: rgba(255,255,255,0.2);
	    color: #fff;
	}

	#cspio-credit img{
		margin-left:auto;
		margin-right:auto;
		width:125px;
		    margin-top: -4px;
	}

	#cspio-credit {
		font-size:11px;
	}
  #overlay {
    position: absolute; /* Sit on top of the page content */
    display: block; /* Hidden by default */
    width: 100%; /* Full width (cover the whole page) */
    height: 100%; /* Full height (cover the whole page) */
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgb(132, 132, 132); /* Black background with opacity */
    z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
    cursor: pointer; /* Add a pointer on hover */
  }
  .text_ech
  {
    text-align: justify;
    font-weight: 700;
    color: #e5e5ff;
  }
  #cspio-socialprofiles 
  {
     margin-top: 60px;
  }
  #cspio-socialprofiles a
  {
     text-decoration: none;
     font-weight: 600;
  }

  #cspio-socialprofiles a:hover
  {
     text-decoration: none;

  }
	</style>

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- Modernizr -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	
	<!-- Google Analytics Code Goes Here-->
</head>
<body>
  <div id="loader"></div>  
  <div id="overlay">

  </div>
	<div id="cspio-page">
		<div id="cspio-content" style="padding-top: 0;">
			
			<img id="cspio-logo" src="images/logo.png">
			    				
			
      <div id="cspio-description"  class=" text_ech" style=" margin-bottom: 20px;">
        Merci de votre visite. Notre site Internet est actuellement en cours de rénovation. Nous serons de retour très prochainement.
				Pour tout besoin d’achat/vente, location ou vol charter, merci de nous contacter.</div>			    				
		
			<div class=" text_ech">
      Thank you for your visit. Our Website is currently being renovated. We will be back soon.
In the mean time for any aircraft sale/purchase, leasing or charter requirement, please feel free to contact us.
      </div>
			    						    			    								    			    			    				
			<div id="cspio-socialprofiles">
        <p><a href="javascript:void(0)" ><i class="fa fa-phone "></i>
          <span>+ 44 20 3608 9883  / +33 6 20 31 19 03 / + 241 62 24 24 02 </span>
        </a></p>			
        <p><a href="javascript:void(0)" ><i class="fa fa-envelope "></i>
          <span>contact@vms.aero, ivanmengome@vms.aero</span>
        </a></p>			

        <p><a href="javascript:void(0)" ><i class="fa fa-whatsapp"></i>
          <span> + 241 62 24 24 02</span>
        </a></p>			
			</div>

									    									    			    			    							    			     			    		
		</div><!-- end of #cspio-content -->
	</div>


	<script>
		// Animate Delay
		setTimeout(function(){ jQuery("#cspio-content").show().addClass('animated fadeIn'); }, 250);

    $(window).load(function() {
      $('#loader').css('display','none')
      $('#overlay').css('z-index',0)
      $('#overlay').css('background-color','rgba(0,0,0,0.5)')
     });
     
		// Reseize	
		function resize(){
				$('head').append("<style id='form-style' type='text/css'></style>");
				$('#form-style').html('.cspio .input-group-btn, .cspio .input-group{display:block;width:100%;}.cspio #cspio-subscribe-btn{margin-left:0;width:100%;display:block;}.cspio .input-group .form-control:first-child, .cspio .input-group-addon:first-child, .cspio .input-group-btn:first-child>.btn, .cspio .input-group-btn:first-child>.dropdown-toggle, .cspio .input-group-btn:last-child>.btn:not(:last-child):not(.dropdown-toggle) {border-bottom-right-radius: 4px;border-top-right-radius: 4px;}.cspio .input-group .form-control:last-child, .cspio .input-group-addon:last-child, .cspio .input-group-btn:last-child>.btn, .cspio .input-group-btn:last-child>.dropdown-toggle, .cspio .input-group-btn:first-child>.btn:not(:first-child) {border-bottom-left-radius: 4px;border-top-left-radius: 4px;}');
		}
		
		$('#cspio-content').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
			function(){
				var width = $('#cspio-field-wrapper').width();
				if(width < 480 && width != 0){
					resize();
				}
			}
		);
	</script>

	</body>
</html>

<!-- This page was generated by ComingSoonPage.com | Learn more: http://www.comingsoonpage.com -->

update  `wp3j_options` set option_value = REPLACE(option_value,'http://www.hts-pro.com','https://www.hts-pro.com');
update  `wp3j_postmeta` set meta_value = REPLACE(meta_value,'http://www.hts-pro.com','https://www.hts-pro.com');
update  `wp3j_posts` set post_content = REPLACE(post_content,'http://www.hts-pro.com','https://www.hts-pro.com'),guid  = REPLACE(guid,'http://www.hts-pro.com','https://www.hts-pro.com');
update  `wp3j_usermeta` set meta_value = REPLACE(meta_value,'http://www.hts-pro.com','https://www.hts-pro.com');
update  `wp3j_wc_admin_notes` set content = REPLACE(content,'http://www.hts-pro.com','https://www.hts-pro.com');

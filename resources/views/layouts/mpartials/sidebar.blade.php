<aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <router-link to="/admin/" class="brand-link">
          <img src="{{ asset('../images/icon.png') }}" alt="AdminLTE Logo" class="brand-image img-circle "
            style="">  
        <span class="brand-text title-header">VMS AERO</span>
        </router-link>

        <!-- Sidebar -->
        <div class="sidebar">
        @if(Auth::check())
        <div id="sidebar-user" class="user-panel  mt-2 pb-3 mb-3 d-flex">
          <div class="pull-left image pt-1">
            @if(Auth::user()->avatar == null)
                <img   src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUPDw8VFRUVFQ8VFRUVFRUVFRUVFRUWFhUVFRUYHSggGBolHRUXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDysZFRkrLTctLS0rKysrLSsrKy03Ny0rNy0rKzctLS0tKystKysrLSstNzcrKy0rLSs3LSsrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQIDBAUH/8QALxABAQACAAIIBQMFAQAAAAAAAAECEQSRAxQhMVFhcbEyQYHB8BKh0QUiQlLhgv/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP1dYLGkIuhQNLpdLoE0ulXQJpdKaBNLpdGgTRprQDOjTQDOk01o0DOk02mgY0mm9JoGNJpuxLAYSt1mwGUaSggANSLIRYCrIRqQEkaNKAppQRdKAmlFBDSgJoUBEaAZ0jQDOka0gM6StaSwGKmm7GaDFiN1mgyKoLGokagEahFAWEaBFFABQRMspJu3U8b2OfEdPMJ4290+98nz+kzuV3ld+09ID258ZjO6W/tP3crxt/1nOvMA9M43L/Wc66Ycbj85Z+7xAPq4ZzLtllV8rHKy7l1Xv4biP1dl7/f0B2FQERpAZRpKDNStVKDFZrdSgwooLGokagLFhFAUUAFAZ6TOYy5XujTyf1DPux+t+nZPzyB5Msrbbe+/mp5IAgAAAAsuu2IA+n0PSfqx3z9XR4uAz/uuPjP3n/HtFQVASo0lBlK1UoMVK1WaDI1pAajSRYCtJFgKCgAoD53G3fSXymM+/wB30XzOK+PL6e0EcgAAAAAAAdOGus8fX37H1Hyuh+LH1x931RUFQEFQEZrSUGazW6zQZFAWNRIsBYooCooEUBB87jZrO+cxv2+z6Lxf1DH4b6z7/wAivIAIAAAAAA68LN54+vtNvpvBwGP91vhPd7xRFARFAZRpAZqVqs0GdCgLGozGoCxUWApAgKAIOPF4bwvl2z6fldgV8cb6bo/05WcvT5MCAAAAALjjbZJ8+wHv4HDWG/G7+ndPu9CSSTU7oooAIlAFSpVqUEZrVZoIACxqMxqAsVIoKQAUAAAHl4/o9z9U+Xf6fnu8L69m+yvldLh+nK4+Ht8gZAEAAHr4Do+25fSevz/PN5ZN3U+fY+r0eExkxny/KK0AAAIlCgqVKtSgjNarNBAAWLGY1AaVlQaEUFEUQAFHz+P+P/zPevX0/T44d/f8pO//AJHzc8rlble+ggAgADrwvx4+v2fTfHlfR6DiZl2Xsvh4+gruAAAIiKgoioCVmqlBBAFjUYjUBqLGWoCqjy9Nxnyw5/L6eIPW558RhO/Ke/s+bnlcvitvr3cu5BHtz46f442+vZ/1wz4rO/PXp/LiAAAAAAAAA69HxGePdd+V7Xow46f5Y8u14gH08OJwv+XPs93R8hcbZ3Wz07AfWSvH0PF3uy5/y9coolVASs1azQBACNSsRqA2sZiwHLjOk1jqfPs+nz/PN4Xo469uM8r7z+HnAAEAAAAAAAAAAAAAAHs4LPsuPh2z0eN34O/3/SivbUolBKlKlAQQCNRiNQG4sZiwHl4z4p6fdweriOiuVlnh4ufVc/LmDiO3Vc/LmdVz8uYOI79Vz8JzOq5+E5g4Dv1TPwnM6pn4TmDgO/VM/CczqmfhOYOA79Uz8JzOqZ+E5g4Dv1TPwnM6pn4TmDgO/Vc/CczqufhOYOA7dVz8uZ1XPy5g4u3C/HPr7HVs/Lm6dB0OWOW7r5/MHoqUSglSlSgCAJGowsBuNRiLsG5VZlXYNKztQaENg0IAomzYKJsBRAAQAQ2gCUSgVmlqUCs1azQFQBIrK7BqNbYXYNyrtiVrYNbXbMq7Bra7YXYNbXbOzYNDOwGhnZsGkTZsF2m02bA2mzaWgWpabTYG0tRNgJTaACAKACrABZ+fu1AAaiAKoAKAH57AAAAIACAAzQAQAZoAMiAMgCP/2Q==" alt="User Image" class="user-image">
            @else
            <!-- <img  src="{{ '/storage/avatars/'.Auth::user()->avatar }}" alt="User Image" class="user-image"> -->
              <img   src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUPDw8VFRUVFQ8VFRUVFRUVFRUVFRUWFhUVFRUYHSggGBolHRUXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDysZFRkrLTctLS0rKysrLSsrKy03Ny0rNy0rKzctLS0tKystKysrLSstNzcrKy0rLSs3LSsrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQIDBAUH/8QALxABAQACAAIIBQMFAQAAAAAAAAECEQSRAxQhMVFhcbEyQYHB8BKh0QUiQlLhgv/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP1dYLGkIuhQNLpdLoE0ulXQJpdKaBNLpdGgTRprQDOjTQDOk01o0DOk02mgY0mm9JoGNJpuxLAYSt1mwGUaSggANSLIRYCrIRqQEkaNKAppQRdKAmlFBDSgJoUBEaAZ0jQDOka0gM6StaSwGKmm7GaDFiN1mgyKoLGokagEahFAWEaBFFABQRMspJu3U8b2OfEdPMJ4290+98nz+kzuV3ld+09ID258ZjO6W/tP3crxt/1nOvMA9M43L/Wc66Ycbj85Z+7xAPq4ZzLtllV8rHKy7l1Xv4biP1dl7/f0B2FQERpAZRpKDNStVKDFZrdSgwooLGokagLFhFAUUAFAZ6TOYy5XujTyf1DPux+t+nZPzyB5Msrbbe+/mp5IAgAAAAsuu2IA+n0PSfqx3z9XR4uAz/uuPjP3n/HtFQVASo0lBlK1UoMVK1WaDI1pAajSRYCtJFgKCgAoD53G3fSXymM+/wB30XzOK+PL6e0EcgAAAAAAAdOGus8fX37H1Hyuh+LH1x931RUFQEFQEZrSUGazW6zQZFAWNRIsBYooCooEUBB87jZrO+cxv2+z6Lxf1DH4b6z7/wAivIAIAAAAAA68LN54+vtNvpvBwGP91vhPd7xRFARFAZRpAZqVqs0GdCgLGozGoCxUWApAgKAIOPF4bwvl2z6fldgV8cb6bo/05WcvT5MCAAAAALjjbZJ8+wHv4HDWG/G7+ndPu9CSSTU7oooAIlAFSpVqUEZrVZoIACxqMxqAsVIoKQAUAAAHl4/o9z9U+Xf6fnu8L69m+yvldLh+nK4+Ht8gZAEAAHr4Do+25fSevz/PN5ZN3U+fY+r0eExkxny/KK0AAAIlCgqVKtSgjNarNBAAWLGY1AaVlQaEUFEUQAFHz+P+P/zPevX0/T44d/f8pO//AJHzc8rlble+ggAgADrwvx4+v2fTfHlfR6DiZl2Xsvh4+gruAAAIiKgoioCVmqlBBAFjUYjUBqLGWoCqjy9Nxnyw5/L6eIPW558RhO/Ke/s+bnlcvitvr3cu5BHtz46f442+vZ/1wz4rO/PXp/LiAAAAAAAAA69HxGePdd+V7Xow46f5Y8u14gH08OJwv+XPs93R8hcbZ3Wz07AfWSvH0PF3uy5/y9coolVASs1azQBACNSsRqA2sZiwHLjOk1jqfPs+nz/PN4Xo469uM8r7z+HnAAEAAAAAAAAAAAAAAHs4LPsuPh2z0eN34O/3/SivbUolBKlKlAQQCNRiNQG4sZiwHl4z4p6fdweriOiuVlnh4ufVc/LmDiO3Vc/LmdVz8uYOI79Vz8JzOq5+E5g4Dv1TPwnM6pn4TmDgO/VM/CczqmfhOYOA79Uz8JzOqZ+E5g4Dv1TPwnM6pn4TmDgO/Vc/CczqufhOYOA7dVz8uZ1XPy5g4u3C/HPr7HVs/Lm6dB0OWOW7r5/MHoqUSglSlSgCAJGowsBuNRiLsG5VZlXYNKztQaENg0IAomzYKJsBRAAQAQ2gCUSgVmlqUCs1azQFQBIrK7BqNbYXYNyrtiVrYNbXbMq7Bra7YXYNbXbOzYNDOwGhnZsGkTZsF2m02bA2mzaWgWpabTYG0tRNgJTaACAKACrABZ+fu1AAaiAKoAKAH57AAAAIACAAzQAQAZoAMiAMgCP/2Q==" alt="User Image" class="user-image">

            @endif  
          </div>
          <div class="pull-left info pt-0">
              <p>{{ Auth::user()->name }}</p> <a href="#"><i class="fa fa-circle" style="color: rgb(60, 118, 61);"></i> Online</a></div>
      </div>
      @endif
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                <li class="nav-item">
                <router-link to="/admin/dashboard" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                      Dashboard
                      <span class="right badge badge-danger"></span>
                  </p>
                </router-link>
            </li>
            <li class="nav-item">
                <router-link to="/admin/companies" class="nav-link">
                <i class="nav-icon fas fa-book"></i>
                <p>
                    Companies
                    <span class="right badge badge-danger"></span>
                </p>
                </router-link>
            </li>
            <li class="nav-item" style="display:none">
                <router-link to="/admin/contacts" class="nav-link">
                <i class="nav-icon fas fa-phone"></i>
                <p>
                    Contacts
                    <span class="right badge badge-danger"></span>
                </p>
                </router-link>
            </li>
            <li class="nav-item" style="display:none">
                <router-link to="/admin/fleets" class="nav-link">
                <i class="nav-icon las la-paper-plane"></i>
                <p>
                    Fleets
                    <span class="right badge badge-danger"></span>
                </p>
                </router-link>
            </li>

            <li class="nav-header"></li>
            <li class="nav-item has-treeview">
            <a href="javascript:void(0)" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
              Settings
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display:none;">
              <li class="nav-item">
                <router-link to="/admin/countries" class="nav-link">
                  <i class="las la-flag nav-icon"></i>
                  <p>Countries</p>
                </router-link>
              </li>
                <li class="nav-item">
                  <router-link to="/admin/aircraft_manufacturers" class="nav-link">
                    <i class="nav-icon las la-plane"></i>
                    <p>Aircraft Manufacturers</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/aircrafts" class="nav-link">
                    <i class="nav-icon las la-plane"></i>
                    <p>Aircraft Types</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/aircrafts_type_variants" class="nav-link">
                  <i class="nav-icon las la-plane"></i>
                    <p>Aircraft Variants</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/aircrafts_type_variant_extensions" class="nav-link">
                  <i class="nav-icon las la-plane"></i>
                    <p>Aircraft Extensions</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/engine_manufacturers" class="nav-link">
                  <i class="nav-icon las la-car-battery"></i>
                    <p>Engine Manufacturers</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/engines" class="nav-link">
                    <i class="nav-icon las la-car-battery"></i>
                    <p>Engine Types</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/engines_type_variants" class="nav-link">
                  <i class="nav-icon las la-car-battery"></i>
                    <p>Engine Variants</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/engines_type_variant_extensions" class="nav-link">
                  <i class="nav-icon las la-car-battery"></i>
                    <p>Engine Extensions</p>
                  </router-link>
                </li>
              <!-- <li class="nav-item">
                <router-link to="/historiques" class="nav-link">
                  <i class="las la-history  nav-icon"></i>
                  <p>History</p>
                </router-link>
              </li> -->
              <li class="nav-item">
                <router-link to="/admin/users" class="nav-link">
                  <i class="fas fa-users nav-icon"></i>
                  <p>Users</p>
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/admin/token" class="nav-link">
                <i class="las la-share-alt nav-icon"></i>
                  <p>API</p>
                </router-link>
              </li>
            </ul>
          </li>

            


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
</aside>
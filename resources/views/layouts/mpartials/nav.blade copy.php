<nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>

        </ul>

        <!-- SEARCH FORM -->
        <search> 
        </search>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <div class="navbar-custom-menu">
              @if(Auth::check())
              <ul class="nav navbar-nav">
                  <li  class="dropdown user user-menu">
                      <a id="user-image-little"  href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false">
                      @if(!Auth::check())
                            <img   src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUPDw8VFRUVFQ8VFRUVFRUVFRUVFRUWFhUVFRUYHSggGBolHRUXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDysZFRkrLTctLS0rKysrLSsrKy03Ny0rNy0rKzctLS0tKystKysrLSstNzcrKy0rLSs3LSsrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQIDBAUH/8QALxABAQACAAIIBQMFAQAAAAAAAAECEQSRAxQhMVFhcbEyQYHB8BKh0QUiQlLhgv/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP1dYLGkIuhQNLpdLoE0ulXQJpdKaBNLpdGgTRprQDOjTQDOk01o0DOk02mgY0mm9JoGNJpuxLAYSt1mwGUaSggANSLIRYCrIRqQEkaNKAppQRdKAmlFBDSgJoUBEaAZ0jQDOka0gM6StaSwGKmm7GaDFiN1mgyKoLGokagEahFAWEaBFFABQRMspJu3U8b2OfEdPMJ4290+98nz+kzuV3ld+09ID258ZjO6W/tP3crxt/1nOvMA9M43L/Wc66Ycbj85Z+7xAPq4ZzLtllV8rHKy7l1Xv4biP1dl7/f0B2FQERpAZRpKDNStVKDFZrdSgwooLGokagLFhFAUUAFAZ6TOYy5XujTyf1DPux+t+nZPzyB5Msrbbe+/mp5IAgAAAAsuu2IA+n0PSfqx3z9XR4uAz/uuPjP3n/HtFQVASo0lBlK1UoMVK1WaDI1pAajSRYCtJFgKCgAoD53G3fSXymM+/wB30XzOK+PL6e0EcgAAAAAAAdOGus8fX37H1Hyuh+LH1x931RUFQEFQEZrSUGazW6zQZFAWNRIsBYooCooEUBB87jZrO+cxv2+z6Lxf1DH4b6z7/wAivIAIAAAAAA68LN54+vtNvpvBwGP91vhPd7xRFARFAZRpAZqVqs0GdCgLGozGoCxUWApAgKAIOPF4bwvl2z6fldgV8cb6bo/05WcvT5MCAAAAALjjbZJ8+wHv4HDWG/G7+ndPu9CSSTU7oooAIlAFSpVqUEZrVZoIACxqMxqAsVIoKQAUAAAHl4/o9z9U+Xf6fnu8L69m+yvldLh+nK4+Ht8gZAEAAHr4Do+25fSevz/PN5ZN3U+fY+r0eExkxny/KK0AAAIlCgqVKtSgjNarNBAAWLGY1AaVlQaEUFEUQAFHz+P+P/zPevX0/T44d/f8pO//AJHzc8rlble+ggAgADrwvx4+v2fTfHlfR6DiZl2Xsvh4+gruAAAIiKgoioCVmqlBBAFjUYjUBqLGWoCqjy9Nxnyw5/L6eIPW558RhO/Ke/s+bnlcvitvr3cu5BHtz46f442+vZ/1wz4rO/PXp/LiAAAAAAAAA69HxGePdd+V7Xow46f5Y8u14gH08OJwv+XPs93R8hcbZ3Wz07AfWSvH0PF3uy5/y9coolVASs1azQBACNSsRqA2sZiwHLjOk1jqfPs+nz/PN4Xo469uM8r7z+HnAAEAAAAAAAAAAAAAAHs4LPsuPh2z0eN34O/3/SivbUolBKlKlAQQCNRiNQG4sZiwHl4z4p6fdweriOiuVlnh4ufVc/LmDiO3Vc/LmdVz8uYOI79Vz8JzOq5+E5g4Dv1TPwnM6pn4TmDgO/VM/CczqmfhOYOA79Uz8JzOqZ+E5g4Dv1TPwnM6pn4TmDgO/Vc/CczqufhOYOA7dVz8uZ1XPy5g4u3C/HPr7HVs/Lm6dB0OWOW7r5/MHoqUSglSlSgCAJGowsBuNRiLsG5VZlXYNKztQaENg0IAomzYKJsBRAAQAQ2gCUSgVmlqUCs1azQFQBIrK7BqNbYXYNyrtiVrYNbXbMq7Bra7YXYNbXbOzYNDOwGhnZsGkTZsF2m02bA2mzaWgWpabTYG0tRNgJTaACAKACrABZ+fu1AAaiAKoAKAH57AAAAIACAAzQAQAZoAMiAMgCP/2Q==" alt="User Image" class="user-image">
                           @else
                             <!-- <img  src="{{ '/storage/avatars/'.Auth::user()->avatar }}" alt="User Image" class="user-image"> -->
                            <img   src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUPDw8VFRUVFQ8VFRUVFRUVFRUVFRUWFhUVFRUYHSggGBolHRUXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDysZFRkrLTctLS0rKysrLSsrKy03Ny0rNy0rKzctLS0tKystKysrLSstNzcrKy0rLSs3LSsrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQIDBAUH/8QALxABAQACAAIIBQMFAQAAAAAAAAECEQSRAxQhMVFhcbEyQYHB8BKh0QUiQlLhgv/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP1dYLGkIuhQNLpdLoE0ulXQJpdKaBNLpdGgTRprQDOjTQDOk01o0DOk02mgY0mm9JoGNJpuxLAYSt1mwGUaSggANSLIRYCrIRqQEkaNKAppQRdKAmlFBDSgJoUBEaAZ0jQDOka0gM6StaSwGKmm7GaDFiN1mgyKoLGokagEahFAWEaBFFABQRMspJu3U8b2OfEdPMJ4290+98nz+kzuV3ld+09ID258ZjO6W/tP3crxt/1nOvMA9M43L/Wc66Ycbj85Z+7xAPq4ZzLtllV8rHKy7l1Xv4biP1dl7/f0B2FQERpAZRpKDNStVKDFZrdSgwooLGokagLFhFAUUAFAZ6TOYy5XujTyf1DPux+t+nZPzyB5Msrbbe+/mp5IAgAAAAsuu2IA+n0PSfqx3z9XR4uAz/uuPjP3n/HtFQVASo0lBlK1UoMVK1WaDI1pAajSRYCtJFgKCgAoD53G3fSXymM+/wB30XzOK+PL6e0EcgAAAAAAAdOGus8fX37H1Hyuh+LH1x931RUFQEFQEZrSUGazW6zQZFAWNRIsBYooCooEUBB87jZrO+cxv2+z6Lxf1DH4b6z7/wAivIAIAAAAAA68LN54+vtNvpvBwGP91vhPd7xRFARFAZRpAZqVqs0GdCgLGozGoCxUWApAgKAIOPF4bwvl2z6fldgV8cb6bo/05WcvT5MCAAAAALjjbZJ8+wHv4HDWG/G7+ndPu9CSSTU7oooAIlAFSpVqUEZrVZoIACxqMxqAsVIoKQAUAAAHl4/o9z9U+Xf6fnu8L69m+yvldLh+nK4+Ht8gZAEAAHr4Do+25fSevz/PN5ZN3U+fY+r0eExkxny/KK0AAAIlCgqVKtSgjNarNBAAWLGY1AaVlQaEUFEUQAFHz+P+P/zPevX0/T44d/f8pO//AJHzc8rlble+ggAgADrwvx4+v2fTfHlfR6DiZl2Xsvh4+gruAAAIiKgoioCVmqlBBAFjUYjUBqLGWoCqjy9Nxnyw5/L6eIPW558RhO/Ke/s+bnlcvitvr3cu5BHtz46f442+vZ/1wz4rO/PXp/LiAAAAAAAAA69HxGePdd+V7Xow46f5Y8u14gH08OJwv+XPs93R8hcbZ3Wz07AfWSvH0PF3uy5/y9coolVASs1azQBACNSsRqA2sZiwHLjOk1jqfPs+nz/PN4Xo469uM8r7z+HnAAEAAAAAAAAAAAAAAHs4LPsuPh2z0eN34O/3/SivbUolBKlKlAQQCNRiNQG4sZiwHl4z4p6fdweriOiuVlnh4ufVc/LmDiO3Vc/LmdVz8uYOI79Vz8JzOq5+E5g4Dv1TPwnM6pn4TmDgO/VM/CczqmfhOYOA79Uz8JzOqZ+E5g4Dv1TPwnM6pn4TmDgO/Vc/CczqufhOYOA7dVz8uZ1XPy5g4u3C/HPr7HVs/Lm6dB0OWOW7r5/MHoqUSglSlSgCAJGowsBuNRiLsG5VZlXYNKztQaENg0IAomzYKJsBRAAQAQ2gCUSgVmlqUCs1azQFQBIrK7BqNbYXYNyrtiVrYNbXbMq7Bra7YXYNbXbOzYNDOwGhnZsGkTZsF2m02bA2mzaWgWpabTYG0tRNgJTaACAKACrABZ+fu1AAaiAKoAKAH57AAAAIACAAzQAQAZoAMiAMgCP/2Q==" alt="User Image" class="user-image">

                     @endif                         
                     <span class="hidden-xs text-gray">{{ Auth::user()->name }}</span>
                      </a>
                      <ul class="dropdown-menu">
                          <li id="user-image-big"  class="user-header">
                           @if(Auth::user()==null && Auth::user()->avatar == null)
                            <img  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUPDw8VFRUVFQ8VFRUVFRUVFRUVFRUWFhUVFRUYHSggGBolHRUXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDysZFRkrLTctLS0rKysrLSsrKy03Ny0rNy0rKzctLS0tKystKysrLSstNzcrKy0rLSs3LSsrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQIDBAUH/8QALxABAQACAAIIBQMFAQAAAAAAAAECEQSRAxQhMVFhcbEyQYHB8BKh0QUiQlLhgv/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP1dYLGkIuhQNLpdLoE0ulXQJpdKaBNLpdGgTRprQDOjTQDOk01o0DOk02mgY0mm9JoGNJpuxLAYSt1mwGUaSggANSLIRYCrIRqQEkaNKAppQRdKAmlFBDSgJoUBEaAZ0jQDOka0gM6StaSwGKmm7GaDFiN1mgyKoLGokagEahFAWEaBFFABQRMspJu3U8b2OfEdPMJ4290+98nz+kzuV3ld+09ID258ZjO6W/tP3crxt/1nOvMA9M43L/Wc66Ycbj85Z+7xAPq4ZzLtllV8rHKy7l1Xv4biP1dl7/f0B2FQERpAZRpKDNStVKDFZrdSgwooLGokagLFhFAUUAFAZ6TOYy5XujTyf1DPux+t+nZPzyB5Msrbbe+/mp5IAgAAAAsuu2IA+n0PSfqx3z9XR4uAz/uuPjP3n/HtFQVASo0lBlK1UoMVK1WaDI1pAajSRYCtJFgKCgAoD53G3fSXymM+/wB30XzOK+PL6e0EcgAAAAAAAdOGus8fX37H1Hyuh+LH1x931RUFQEFQEZrSUGazW6zQZFAWNRIsBYooCooEUBB87jZrO+cxv2+z6Lxf1DH4b6z7/wAivIAIAAAAAA68LN54+vtNvpvBwGP91vhPd7xRFARFAZRpAZqVqs0GdCgLGozGoCxUWApAgKAIOPF4bwvl2z6fldgV8cb6bo/05WcvT5MCAAAAALjjbZJ8+wHv4HDWG/G7+ndPu9CSSTU7oooAIlAFSpVqUEZrVZoIACxqMxqAsVIoKQAUAAAHl4/o9z9U+Xf6fnu8L69m+yvldLh+nK4+Ht8gZAEAAHr4Do+25fSevz/PN5ZN3U+fY+r0eExkxny/KK0AAAIlCgqVKtSgjNarNBAAWLGY1AaVlQaEUFEUQAFHz+P+P/zPevX0/T44d/f8pO//AJHzc8rlble+ggAgADrwvx4+v2fTfHlfR6DiZl2Xsvh4+gruAAAIiKgoioCVmqlBBAFjUYjUBqLGWoCqjy9Nxnyw5/L6eIPW558RhO/Ke/s+bnlcvitvr3cu5BHtz46f442+vZ/1wz4rO/PXp/LiAAAAAAAAA69HxGePdd+V7Xow46f5Y8u14gH08OJwv+XPs93R8hcbZ3Wz07AfWSvH0PF3uy5/y9coolVASs1azQBACNSsRqA2sZiwHLjOk1jqfPs+nz/PN4Xo469uM8r7z+HnAAEAAAAAAAAAAAAAAHs4LPsuPh2z0eN34O/3/SivbUolBKlKlAQQCNRiNQG4sZiwHl4z4p6fdweriOiuVlnh4ufVc/LmDiO3Vc/LmdVz8uYOI79Vz8JzOq5+E5g4Dv1TPwnM6pn4TmDgO/VM/CczqmfhOYOA79Uz8JzOqZ+E5g4Dv1TPwnM6pn4TmDgO/Vc/CczqufhOYOA7dVz8uZ1XPy5g4u3C/HPr7HVs/Lm6dB0OWOW7r5/MHoqUSglSlSgCAJGowsBuNRiLsG5VZlXYNKztQaENg0IAomzYKJsBRAAQAQ2gCUSgVmlqUCs1azQFQBIrK7BqNbYXYNyrtiVrYNbXbMq7Bra7YXYNbXbOzYNDOwGhnZsGkTZsF2m02bA2mzaWgWpabTYG0tRNgJTaACAKACrABZ+fu1AAaiAKoAKAH57AAAAIACAAzQAQAZoAMiAMgCP/2Q==" alt="User Image" class="img-circle">
                           @else
                             <!-- <img  src="{{ '/storage/avatars/'.Auth::user()->avatar }}" alt="User Image" class="img-circle"> -->
                            <img   src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxUPDw8VFRUVFQ8VFRUVFRUVFRUVFRUWFhUVFRUYHSggGBolHRUXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDQ0NDg0NDysZFRkrLTctLS0rKysrLSsrKy03Ny0rNy0rKzctLS0tKystKysrLSstNzcrKy0rLSs3LSsrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAQIDBAUH/8QALxABAQACAAIIBQMFAQAAAAAAAAECEQSRAxQhMVFhcbEyQYHB8BKh0QUiQlLhgv/EABYBAQEBAAAAAAAAAAAAAAAAAAABAv/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP1dYLGkIuhQNLpdLoE0ulXQJpdKaBNLpdGgTRprQDOjTQDOk01o0DOk02mgY0mm9JoGNJpuxLAYSt1mwGUaSggANSLIRYCrIRqQEkaNKAppQRdKAmlFBDSgJoUBEaAZ0jQDOka0gM6StaSwGKmm7GaDFiN1mgyKoLGokagEahFAWEaBFFABQRMspJu3U8b2OfEdPMJ4290+98nz+kzuV3ld+09ID258ZjO6W/tP3crxt/1nOvMA9M43L/Wc66Ycbj85Z+7xAPq4ZzLtllV8rHKy7l1Xv4biP1dl7/f0B2FQERpAZRpKDNStVKDFZrdSgwooLGokagLFhFAUUAFAZ6TOYy5XujTyf1DPux+t+nZPzyB5Msrbbe+/mp5IAgAAAAsuu2IA+n0PSfqx3z9XR4uAz/uuPjP3n/HtFQVASo0lBlK1UoMVK1WaDI1pAajSRYCtJFgKCgAoD53G3fSXymM+/wB30XzOK+PL6e0EcgAAAAAAAdOGus8fX37H1Hyuh+LH1x931RUFQEFQEZrSUGazW6zQZFAWNRIsBYooCooEUBB87jZrO+cxv2+z6Lxf1DH4b6z7/wAivIAIAAAAAA68LN54+vtNvpvBwGP91vhPd7xRFARFAZRpAZqVqs0GdCgLGozGoCxUWApAgKAIOPF4bwvl2z6fldgV8cb6bo/05WcvT5MCAAAAALjjbZJ8+wHv4HDWG/G7+ndPu9CSSTU7oooAIlAFSpVqUEZrVZoIACxqMxqAsVIoKQAUAAAHl4/o9z9U+Xf6fnu8L69m+yvldLh+nK4+Ht8gZAEAAHr4Do+25fSevz/PN5ZN3U+fY+r0eExkxny/KK0AAAIlCgqVKtSgjNarNBAAWLGY1AaVlQaEUFEUQAFHz+P+P/zPevX0/T44d/f8pO//AJHzc8rlble+ggAgADrwvx4+v2fTfHlfR6DiZl2Xsvh4+gruAAAIiKgoioCVmqlBBAFjUYjUBqLGWoCqjy9Nxnyw5/L6eIPW558RhO/Ke/s+bnlcvitvr3cu5BHtz46f442+vZ/1wz4rO/PXp/LiAAAAAAAAA69HxGePdd+V7Xow46f5Y8u14gH08OJwv+XPs93R8hcbZ3Wz07AfWSvH0PF3uy5/y9coolVASs1azQBACNSsRqA2sZiwHLjOk1jqfPs+nz/PN4Xo469uM8r7z+HnAAEAAAAAAAAAAAAAAHs4LPsuPh2z0eN34O/3/SivbUolBKlKlAQQCNRiNQG4sZiwHl4z4p6fdweriOiuVlnh4ufVc/LmDiO3Vc/LmdVz8uYOI79Vz8JzOq5+E5g4Dv1TPwnM6pn4TmDgO/VM/CczqmfhOYOA79Uz8JzOqZ+E5g4Dv1TPwnM6pn4TmDgO/Vc/CczqufhOYOA7dVz8uZ1XPy5g4u3C/HPr7HVs/Lm6dB0OWOW7r5/MHoqUSglSlSgCAJGowsBuNRiLsG5VZlXYNKztQaENg0IAomzYKJsBRAAQAQ2gCUSgVmlqUCs1azQFQBIrK7BqNbYXYNyrtiVrYNbXbMq7Bra7YXYNbXbOzYNDOwGhnZsGkTZsF2m02bA2mzaWgWpabTYG0tRNgJTaACAKACrABZ+fu1AAaiAKoAKAH57AAAAIACAAzQAQAZoAMiAMgCP/2Q==" alt="User Image" class="user-image">
                           @endif
                          <p>
                                  {{ Auth::user()->name }}<br>
                                  <small></small>
                          </p>
                          </li>
                          <li class="user-footer">
                              <div class="pull-left">
                                <a to="javascript:void(0)" class="btn btn-default btn-flat"><i class="nav-icon fas fa-user mr-1 "></i>Profil</a>
                              </div>
                              <div class="pull-right">
                                  <button @click="logout" class="btn btn-default btn-flat"> <i class="nav-icon fas fa-power-off text-red mr-1"></i>Déconnexion</button>
                              </div>
                          </li>
                      </ul>
                  </li>
                  <li><a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a></li>
              </ul>
              @endif
          </div>
      </ul>
    </nav>
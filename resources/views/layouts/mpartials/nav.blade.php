<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container pl-2 pr-2">
      <a href="/admin/" class="navbar-brand">
        <!-- <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
        <span class="brand-text font-weight-light">VMS.aero</span>
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <router-link to="/admin/dashboard" class="nav-link"><i class="nav-icon mr-1 fas fa-tachometer mr-1"></i>Dashboard</router-link>
          </li>
          <li class="nav-item">
          <router-link to="/admin/companies" class="nav-link"><i class="nav-icon mr-1 fas fa-book"></i>Companies</router-link>

          </li>

        </ul>

        <!-- SEARCH FORM -->
        <search> 
        </search>
      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <!-- Messages Dropdown Menu -->

        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="nav-icon mr-1 fas fa-cog mr-1"></i> Parameters</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li class="nav-item">
                <router-link to="/admin/countries" class="dropdown-item">
                  <i class="las la-flag nav-icon mr-1"></i>Countries
                  
                </router-link>
              </li>
              <li class="dropdown-divider"></li>
                <li class="nav-item">
                  <router-link to="/admin/aircraft_manufacturers" class="dropdown-item">
                    <i class="nav-icon mr-1 las la-plane"></i>
                    Aircraft Manufacturers
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/aircrafts" class="dropdown-item">
                    <i class="nav-icon mr-1 las la-plane"></i>
                    Aircraft Types
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/aircrafts_type_variants" class="dropdown-item">
                  <i class="nav-icon mr-1 las la-plane"></i>
                    Aircraft Variants
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/aircrafts_type_variant_extensions" class="dropdown-item">
                  <i class="nav-icon mr-1 las la-plane"></i>
                    Aircraft Extensions
                  </router-link>
                </li>
                <li class="dropdown-divider"></li>
                <li class="nav-item">
                  <router-link to="/admin/engine_manufacturers" class="dropdown-item">
                  <i class="nav-icon mr-1 las la-car-battery"></i>
                    Engine Manufacturers
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/engines" class="dropdown-item">
                    <i class="nav-icon mr-1 las la-car-battery"></i>
                    Engine Types
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/engines_type_variants" class="dropdown-item">
                  <i class="nav-icon mr-1 las la-car-battery"></i>
                    Engine Variants
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/admin/engines_type_variant_extensions" class="dropdown-item">
                  <i class="nav-icon mr-1 las la-car-battery"></i>
                    Engine Extensions
                  </router-link>
                </li>
              <!-- <li class="nav-item">
                <router-link to="/historiques" class="nav-link">
                  <i class="las la-history  nav-icon mr-1"></i>
                  <p>History</p>
                </router-link>
              </li> -->
              <li class="dropdown-divider"></li>
              <li class="nav-item">
                <router-link to="/admin/users" class="dropdown-item">
                  <i class="fas fa-users nav-icon mr-1"></i>
                  Users
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/admin/token" class="dropdown-item">
                <i class="las la-share-alt nav-icon mr-1"></i>
                  API
                </router-link>
              </li>
            </ul>
          </li>
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fas fa-user"></i>
            @if(Auth::check())
                               {{ Auth::user()->name }}                           
                          @else
                             <!-- <img  src="{{ '/storage/avatars/'.Auth::user()->avatar }}" alt="User Image" class="user-image"> -->
                             Not logged in
            @endif 
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-header">
                @if(Auth::check())
                               {{ Auth::user()->name }}                           
                          @else
                             <!-- <img  src="{{ '/storage/avatars/'.Auth::user()->avatar }}" alt="User Image" class="user-image"> -->
                             Not logged in
                     @endif    
            </span>
            <div class="dropdown-divider"></div>
            <router-link to="/admin/profile" class="dropdown-item">
               <i class="nav-icon fas fa-user mr-1 "></i>Profile
            </router-link>
            <div class="dropdown-divider"></div>
              <button href="javascript:void(0)" @click="logout" class="dropdown-item dropdown-footer"> <i class="nav-icon fas fa-power-off text-red mr-1"></i> Log out</button>
          </div>
        </li>
      </ul>
    </div>
  </nav>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  @if (auth()->check())
    <meta name="api-token" content="{{ auth()->user()->api_token }}">
  @endif
  <title>Vms Aero</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
      <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.css" integrity="sha256-QVBN0oT74UhpCtEo4Ko+k3sNo+ykJFBBtGduw13V9vw=" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">  
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">


</head>
<body class=" text-sm layout-top-nav">
<div id="app" class="wrapper" v-cloak>
    <!-- Navbar -->
       @include('layouts.mpartials.nav')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <!-- ./Main Sidebar Container -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper pl-3 pr-3">
       <transition name="page" mode="out-in">
          <router-view :key="$route.fullPath"></router-view>
        </transition>
    </div>
    <flash type="{{ session('type') }}" message="{{ session('status') }}">
    
    </flash>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
        
    <!-- /.control-sidebar -->

    <!-- Main Footer -->

        @include('layouts.mpartials.footer')
    <!-- ./Main Footer -->

</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script  type="application/javascript">
         window.App = {!!
          json_encode(array( 'csrfToken' => csrf_token(),'user' => Auth::user(),'api_token' => Auth::user()->api_token,'signedIn' => Auth::check()));
          !!}  

  </script>
<script src="{{ asset('js/app.js') }}" defer></script>

</body>
</html>

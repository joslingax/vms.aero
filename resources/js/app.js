/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./filter');
window.Vue = require('vue');



import VueEvents from 'vue-events'
Vue.use(VueEvents)

import router from './router'
window.Form = require('./classes/Form').default
import { BModal, VBModal } from 'bootstrap-vue'
import FilterBar from './components/FilterBar.vue'
import Search from './components/Search.vue'
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import VueTimeline from "./plugins/@growthbunker/vuetimeline"; 
Vue.use(VueTimeline, {
  // Specify the theme to use: dark or light (dark by default).
  theme: "light",
});
Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);

Vue.component('multi-select', require('vue-multiselect').default)
Vue.component('page-header', require('./components/PageHeader.vue').default);
Vue.component('b-modal', BModal)
Vue.component('flash', require('./components/Flash.vue').default);
Vue.component('no-data-in-search', require('./components/NoDataInSearch.vue').default);
Vue.component('pagination', require('./components/Pagination.vue').default);
Vue.component('filter-bar', FilterBar)


Vue.component(
  'passport-clients',
  require('./components/passport/Clients.vue').default
);

Vue.component(
  'passport-authorized-clients',
  require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
  'passport-personal-access-tokens',
  require('./components/passport/PersonalAccessTokens.vue').default
);

import VCalendar from 'v-calendar';

// Use v-calendar & v-date-picker components
Vue.use(VCalendar, {
  componentPrefix: 'vc',  // Use <vc-calendar /> instead of <v-calendar />
  
  locales: {
    'en-En': {
      firstDayOfWeek: 1,
      masks: {
        L: 'YYYY-MM-DD',
        // ...optional `title`, `weekdays`, `navMonths`, etc
      }
    }
  }
});


import { Datetime } from 'vue-datetime';

Vue.component('datetime', Datetime);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    mounted()
    {
   
    },
    created()
    {
        
    },
    components:
    {
     Search
    },
    data()
    {
      return{
      }   
    },
    methods:{

        logout(){
            axios.post('/admin/logout').then(response => {
                this.$router.go("/admin/login")
                flash("<span>Déconnexion <strong> réussie </strong></span>",'primary')


            }).catch(error => {
                location.reload();
            });
        }
    },
    watch:
     {

           
     }
});

import  Vue  from  "vue";

Vue.filter("capitalize",  function(value)  {
  if  (!value)  {
    return  "";
  }
  value  =  value.toString();
  return  value.charAt(0).toUpperCase()  +  value.slice(1);
});

Vue.filter("cutText",  function(value,  length,  suffix)  {
  if  (value.length  >  length)  {
    return  value.substring(0,  length)  +  suffix;
  }  else  {
    return  value;
  }
});
Vue.filter("toDateTime",  function(value) 
{
  if(value=="" || value==null || value==undefined)
  return ""
  else
   return  toDateFormat(value,"DD-MM-YYYY à hh:mm")
}); 

Vue.filter("toDate",  function(value) 
{
  if(value=="" || value==null || value==undefined)
  return ""
  else
   return  toDateFormat(value,"DD-MM-YYYY")
}); 

Vue.filter("toYear",  function(value) 
{
  if(value=="" || value==null || value==undefined)
  return ""
  else
   return  moment(value,"DD-MM-YYYY").year()
}); 
Vue.filter("monnaie",  function(value) 
{
   return  numeral(value).format('0,0');
});
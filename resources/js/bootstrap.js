window.Vue = require('vue')
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
    require('admin-lte');
} catch (e) {}

Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][prop]
    }
    return total
}

window.moment= require('moment');
window.numeral= require('numeral');
window.toDateFormat = function(date_frmt,frmt)
{
    return moment(date_frmt).format(frmt)
}
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');


window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.App.csrfToken,
    'X-Requested-With': 'XMLHttpRequest',
    'Accept' : 'application/json',
};

// let api_token = document.head.querySelector('meta[name="api-token"]');

// if (api_token) {
//   window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + api_token.content;
// }
window.event = new Vue();
window.monnaie= function(value)
{
   return  numeral(value).format('0,0');
}
window.flash = function(message,type="success")
{
    window.event.$emit("flash",{message : message ,type : type});
}

window.log = function(message)
{
    console.log(message)
}
window.blockUI = require('./plugins/BlockUI.js');
$.blockUI.defaults.css = {}

import jc from 'jquery-confirm';

window.onAlert = function(message,title="Information")
{
    $.alert({
        closeIcon: true,
        closeIconClass: 'fas fa-close',
        title: title,
        theme: 'light',
        animateFromElement: false,
        type: 'blue',
        content: message,
    });
}
window.alertConfirm  = function (message="Vous ne serez pas en mesure de le changer!",title="Voulez-vous supprimer cet élément ?") {
    return new Promise((resolve, reject) =>
    {
        $.confirm
        ({
            closeIcon: true,
            closeIconClass: 'fas fa-close',
            title: title,
            theme: 'light',
            animateFromElement: false,
            type: 'red',
            content: message,
            buttons: {
                tryAgain: {
                    text: 'Confirm',
                    btnClass: 'btn-red',
                    action: function(){
                           resolve(true)
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-default',
                    close: function(){
                        resolve(false)
                    }
                }
            }
        });
    });
}
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
Number.prototype.pad = function(n) {
    return new Array(n).join('0').slice((n || 2) * -1) + this;
}
String.prototype.ucFirst = function(n) {
    return new n.charAt(0).toUpperCase() + n.slice(1)
}

String.prototype.equals = function(n,param) {
    return n==param ? true : false
}



//Verifie si une variable 
window.isDefined = function(elem)
{
    if(elem!= null &&  elem != undefined && elem!="")
     return true 
    else 
     return false
}
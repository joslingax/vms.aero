import Vue from 'vue'
import Router from 'vue-router'



Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/admin/',
      name: 'dashboard',
      component: require('./../pages/Dashboard.vue').default
    },
    {
      path: '/admin/dashboard',
      name: 'dashboard',
      component: require('./../pages/Dashboard.vue').default
    },

    {
      path: '/admin/users',
      name: 'users',
      component: require('./../pages/parametres/Users.vue').default
    },
    {
      path: '/admin/companies',
      name: 'compagnies',
      component: require('./../pages/companies/Companies.vue').default
    },
    {
      path: '/admin/companies/:id',
      name: 'compagnies-show',
      component: require('./../pages/companies/Show.vue').default
    },
    {
      path: '/admin/contacts',
      name: 'contacts',
      component: require('./../pages/contacts/Contacts.vue').default
    },
    {
      path: '/admin/fleets',
      name: 'fleets',
      component: require('./../pages/fleets/Fleets.vue').default
    },
    {
      path: '/admin/aircrafts',
      name: 'aircrafts',
      component: require('./../pages/parametres/aircrafts/Types.vue').default
    },
    {
      path: '/admin/aircraft_manufacturers',
      name: 'aircraft_manufacturers',
      component: require('./../pages/parametres/aircrafts/Manufacturers.vue').default

    },
    {
      path: '/admin/aircrafts_type_variants',
      name: 'aircraft_type_variants',
      component: require('./../pages/parametres/aircrafts/Variants.vue').default

    },
    {
      path: '/admin/aircrafts_type_variant_extensions',
      name: 'aircraft_type_variant_extensions',
      component: require('./../pages/parametres/aircrafts/Extensions.vue').default

    },
    {
      path: '/admin/engines',
      name: 'engines',
      component: require('./../pages/parametres/engines/Types.vue').default
    },
    {
      path: '/admin/engine_manufacturers',
      name: 'engine_manufacturers',
      component: require('./../pages/parametres/engines/Manufacturers.vue').default
  
    },
    {
      path: '/admin/engines_type_variants',
      name: 'engine_type_variants',
      component: require('./../pages/parametres/engines/Variants.vue').default
  
    },
    {
      path: '/admin/engines_type_variant_extensions',
      name: 'engine_type_variant_extensions',
      component: require('./../pages/parametres/engines/Extensions.vue').default
  
    },
    {
      path: '/admin/profil',
      name: 'profil',
      component: require('./../pages/profils/Profil.vue').default

    },
    {
      path: '/admin/token',
      name: 'token',
      component: require('./../pages/parametres/auth/Token.vue').default

    },
    {
      path: '/admin/countries',
      name: 'countries',
      component: require('./../pages/parametres/countries/Countries.vue').default

    },
    {
      path: '/admin/roles',
      name: 'roles',
      component: require('./../pages/parametres/roles/Roles.vue').default

    },
    {
      path: '/admin/roles/ajout',
      name: 'roles',
      component: require('./../pages/parametres/roles/Show.vue').default

    },
    {
      path: '/admin/roles/:id',
      name: 'roles_details',
      component: require('./../pages/parametres/roles/Show.vue').default

    },
    // {
    //   path: '/historiques',
    //   name: 'historiques',
    //   component: require('./../pages/parametres/historique/Historiques.vue').default

    // },
    {
      path: '/admin/users',
      name: 'users',
      component: require('./../pages/parametres/Users.vue').default

    },
    {
      path: '/admin/search/:content',
      name: 'search',
      component: require('./../pages/SearchPage.vue').default
      

    },
    { path: '/admin/404', name: '404', component: require('./../pages/NotFound.vue').default },
    { path: '/admin/*', redirect: '/admin/404' },
  ],
  linkActiveClass: 'active'
})

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\AircraftManufacturer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'country_id' => factory(App\Country::class),

    ];
});

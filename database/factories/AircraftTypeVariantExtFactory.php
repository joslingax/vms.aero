<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\AircraftTypeVariantExt::class, function (Faker $faker) {
    return [
        'label' => $faker->word,
        //'aircraft_type_variant_id' => factory(App\AircraftTypeVariant::class),
    ];
});

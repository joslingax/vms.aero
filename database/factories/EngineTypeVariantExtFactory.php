<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\EngineTypeVariantExt::class, function (Faker $faker) {
    return [
        'label' => $faker->word,
        //'engine_type_variant_id' => factory(App\EngineTypeVariant::class),
    ];
});

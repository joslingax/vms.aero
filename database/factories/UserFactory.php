<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->lastName,
        'email' => $faker->safeEmail,
        'email_verified_at' => $faker->dateTime(),
        'role' => $faker->randomElement(['reader','admin','basic']),
        'password' => bcrypt("secret"),
        'avatar' => $faker->word,
        'api_token' => Str::random(80),
        'remember_token' => Str::random(10),
    ];
});

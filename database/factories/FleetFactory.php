<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Fleet::class, function (Faker $faker) {
    return [
        'serial_number' => $faker->isbn13,
        'registration' => $faker->isbn13,
        'year_of_manufacture' => $faker->year($max = 'now'),
        'company_id' => factory(App\Company::class),
        'engine_id' => factory(App\Engine::class),
        'engine_type_variant_id' => factory(App\EngineTypeVariant::class),
        'engine_type_variant_ext_id' => factory(App\EngineTypeVariantExt::class),

        'aircraft_id' => factory(App\Aircraft::class),
        'aircraft_type_variant_id' => factory(App\AircraftTypeVariant::class),
        'aircraft_type_variant_ext_id' => factory(App\AircraftTypeVariantExt::class),
    ];
});

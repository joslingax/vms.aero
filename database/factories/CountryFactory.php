<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Country;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
$factory->define(App\Country::class, function (Faker $faker) {
    do
    {
      $name = $faker->country;

    }while(Country::whereName($name)->first()!=null);

    return [
        'name' => $name,
        'code' => $faker->countryCode."".Str::random(4),
        'region_id' => factory(App\Region::class),
    ];
});

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\OtherCategory::class, function (Faker $faker) {
    return [
        'label' => $faker->name,

    ];
});

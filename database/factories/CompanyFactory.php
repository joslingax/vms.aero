<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'address' => $faker->address,
        'website' => $faker->domainName,
        'is_airline' => $faker->boolean,
        'is_low_cost' => $faker->boolean,
        'is_acmi_charter' => $faker->boolean,
    ];
});

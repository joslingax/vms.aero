<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;
$factory->define(App\EngineTypeVariant::class, function (Faker $faker) {
    return [
        'label' => $faker->word."".Str::random(4),
        //'engine_id' => factory(App\Engine::class),
    ];
});

<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\AircraftTypeVariant::class, function (Faker $faker) {
    return [
        'label' => $faker->word,
        //'aircraft_id' => factory(App\Aircraft::class),
    ];
});

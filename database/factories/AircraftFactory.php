<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Aircraft::class, function (Faker $faker) {
    return [
        'type' => $faker->word,
        'manufacturer_id' => factory(App\AircraftManufacturer::class),
    ];
});

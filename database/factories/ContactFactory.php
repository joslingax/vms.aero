<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Contact::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName." ".$faker->lastName,
        'title' => $faker->randomElement(['Miss', 'Mr', 'Mrs', 'Dr.', 'Pr.']),
        'position' => $faker->jobTitle,
        // 'cel' => $faker->phoneNumber,
        // 'tel' => $faker->phoneNumber,
        // 'email' => $faker->safeEmail,
        'company_id' => factory(App\Company::class),
    ];
});

$factory->define(App\PhoneNumber::class, function (Faker $faker) {
    return [
        'label' => $faker->firstName,
        'content' => $faker->phoneNumber,
        'is_main' => $faker->boolean(),
        'contact_id' => factory(App\Contact::class),
    ];
});
$factory->define(App\Email::class, function (Faker $faker) {
    return [
        'label' => $faker->firstName,
        'content' => $faker->email,
        'is_main' => $faker->boolean(),
        'contact_id' => factory(App\Contact::class),
    ];
});

$factory->define(App\Fax::class, function (Faker $faker) {
    return [
        'label' => $faker->firstName,
        'content' => $faker->phoneNumber,
        'is_main' => $faker->boolean(),
        'contact_id' => factory(App\Contact::class),
    ];
});
<?php

use App\Company;
use App\Country;
use App\Email;
use App\Fax;
use App\PhoneNumber;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

use function Ramsey\Uuid\v1;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $companies = Config::get('companies');
        foreach(array_chunk($companies,100) as $key => $chunks)
        {
            foreach($chunks as $key => $company)
            {
                            if($company[1]!=""  && $company[1]!=null)
            {
                $country=Country::where('name',mb_strtolower(trim($company[1])))->first();
    
                if($country==null)
                {
                    
                    $data=['name'=>mb_strtolower(trim($company[1])),'region_id'=> null,"code"=>null];
                    $country = factory('App\Country')->create($data);
                }

                $company = factory('App\Company')->create(['name'=>mb_strtolower($company[0]),"country_id"=>$country->id]);
               // array_push($list,$company->toArray());
                $contact=factory('App\Contact')->create(['company_id'=>$company->id]);

                factory('App\PhoneNumber')->create(['label'=>'Phone 1','contact_id'=>$contact->id,'is_main'=>true]);
                factory('App\PhoneNumber')->create(['label'=>'Phone 2','contact_id'=>$contact->id,'is_main'=>false]);
    
                factory('App\Email')->create(['label'=>'Email 1','contact_id'=>$contact->id,'is_main'=>true]);
                factory('App\Email')->create(['label'=>'Email 2','contact_id'=>$contact->id,'is_main'=>false]);
    
                factory('App\Fax')->create(['label'=>'Fax 1','contact_id'=>$contact->id,'is_main'=>true]);
                factory('App\Fax')->create(['label'=>'Fax 2','contact_id'=>$contact->id,'is_main'=>false]);
            }
            }

    
        }
   
        
        }
}

<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $regions =["Africa","Asia","Europe","Middle East","North America","Oceania","South America"];
       foreach($regions as $region)
       {
           $data=(['region_name'=>$region]);
           factory('App\Region')->create($data);
       }
    }
}

<?php

use Illuminate\Database\Seeder;

class AirOperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     $categories = ["Schedule Airline","Low Cost","Charter","Corporate / VIP","Cargo","Air Ambulance"];
    
     foreach($categories as $category)
     {
        factory('App\Category')->create(['label'=>$category]);
     }
    }
}

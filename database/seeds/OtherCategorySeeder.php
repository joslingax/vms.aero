<?php

use Illuminate\Database\Seeder;

class OtherCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

         $categories = ["Travel Agent","Institution","Aircraft Lessor / Owner","Consultant","Broker","Re-saler"];
        
         foreach($categories as $category)
         {
            factory('App\OtherCategory')->create(['label'=>$category]);
         }
    }
}

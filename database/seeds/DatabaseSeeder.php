<?php

use App\AircraftManufacturer;
use App\Company;
use App\Country;
use App\EngineManufacturer;
use App\Region;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory('App\User')->create(["email"=>"joslingax@vmsaero.com","username"=>"joslingax","username"=>"joslingax",'role'=>"admin"]);
        factory('App\User')->create(["email"=>"basic@vmsaero.com","username"=>"basic","name"=>"Mr Basic",'role'=>"basic"]);
        factory('App\User')->create(["email"=>"reader@vmsaero.com","username"=>"reader","name"=>"Mr Guest",'role'=>"reader"]);
        factory('App\User')->create(["email"=>"admin@vmsaero.com","username"=>"admin",'name'=>"Mr Admin",'role'=>"admin"]);

        $regions = ['Middle East','North America',"Oceania","South America","Europe","Africa","Asia"];
        foreach($regions as $region)
        {
            $data=(['region_name'=>mb_strtolower($region)]);
            factory('App\Region')->create($data);
        }

        $countries = Config::get('countries');
        $codes = Config::get('codes');


        $regions = ['Middle East','North America',"Oceania","South America","Europe","Africa","Asia"];
 
            foreach(array_chunk($countries,100) as $key => $chunks)
            {
                $list = [];
                foreach($chunks as $key => $country)
                {
                    $region= $country['region'] != null ? Region::where('region_name',mb_strtolower($country['region']))->first() : null;
        
                    if($region==null )
                    {
                        if($country['region'] != null)
                        $region=factory('App\Region')->create(['region_name'=>mb_strtolower($country['region'])]);
                    }

                    $data=['name'=>mb_strtolower($country['name']),'region_id'=>$region != null ? $region->id :null,"code"=>mb_strtolower($country['code'])];

                    $country = factory('App\Country')->make($data);
                    array_push($list,$country->setAppends([])->toArray());

                }
                Country::insert($list);

           }

    

      /*  $countries = Country::all()->random()->take(10)->get();
        foreach($countries  as $country)
        {
            $n = collect([1,1,2,2])->random();
            factory('App\Company',$n)->create(['country_id'=>$country->id]);
        }

        $companies = Company::all();
        foreach($companies  as $company)
        {
            $n = collect([5,3,4,2,7,1])->random();
            $contact=factory('App\Contact')->create(['company_id'=>$company->id]);

            factory('App\PhoneNumber')->create(['label'=>'Phone 1','contact_id'=>$contact->id,'is_main'=>true]);
            factory('App\PhoneNumber',2)->create(['label'=>'Phone 2','contact_id'=>$contact->id,'is_main'=>false]);

            factory('App\Email')->create(['label'=>'Email 1','contact_id'=>$contact->id,'is_main'=>true]);
            factory('App\Email',2)->create(['label'=>'Email 2','contact_id'=>$contact->id,'is_main'=>false]);

            factory('App\Fax')->create(['label'=>'Fax 1','contact_id'=>$contact->id,'is_main'=>true]);
            factory('App\Fax',2)->create(['label'=>'Fax 2','contact_id'=>$contact->id,'is_main'=>false]);
        }*/

        $aircrafts_ = Config::get('aircrafts');
        $aircrafts = $aircrafts_['aircrafts'];
        foreach($aircrafts as $aircraft)
        {
            $manufacturer = AircraftManufacturer::where('name',mb_strtolower($aircraft[0]))->first();
            $country=Country::where('name',mb_strtolower($aircraft[2]))->first();

            if($manufacturer==null)
            {
                $manufacturer=factory('App\AircraftManufacturer')->create(['name'=>mb_strtolower($aircraft[0]),"country_id"=>$country ? $country->id : null]);
            }

            factory('App\Aircraft')->create(['type'=>$aircraft[1],"manufacturer_id"=>$manufacturer->id]);
        }

        $variants = $aircrafts_['variants'];
        foreach($variants as $variant)
        {
        
            factory('App\AircraftTypeVariant')->create(['label'=>mb_strtolower($variant)]);
        }

        $extensions = $aircrafts_['extensions'];
        foreach($extensions as $extension)
        {
        
            factory('App\AircraftTypeVariantExt')->create(['label'=>$extension]);
        }

        
        $engines_ = Config::get('engines');
        $engines = $engines_['engines'];
        foreach($engines as $engine)
        {
            $manufacturer = EngineManufacturer::where('name',$engine[0])->first();
            $country=Country::where('name',mb_strtolower($engine[2]))->first();

            if($manufacturer==null)
            {
                $manufacturer=factory('App\EngineManufacturer')->create(['name'=>mb_strtolower($engine[0]),"country_id"=>$country ? $country->id : null]);
            }
            factory('App\Engine')->create(['type'=>mb_strtolower($engine[1]),"manufacturer_id"=>$manufacturer->id]);
        }

        $variants = $engines_['variants'];
        foreach($variants as $variant)
        {
        
            factory('App\EngineTypeVariant')->create(['label'=>$variant]);
        }

        $extensions = $engines_['extensions'];
        foreach($extensions as $extension)
        {
        
            factory('App\EngineTypeVariantExt')->create(['label'=>$extension]);
        }

       $this->call([CompanySeeder::class]);


    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('name')->index();
            $table->string('address')->nullable();
            $table->string('website')->nullable();
            $table->boolean('is_airline')->default(false);
            $table->boolean('is_low_cost')->default(false);
            $table->boolean('is_acmi_charter')->default(false);
            $table->UnsignedbigInteger('country_id')->nullable();
            
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}

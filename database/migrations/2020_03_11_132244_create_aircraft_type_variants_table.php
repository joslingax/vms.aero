<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAircraftTypeVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircraft_type_variants', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('label');
            //$table->UnsignedbigInteger('aircraft_id')->nullable();
            //$table->foreign('aircraft_id')->references('id')->on('aircrafts')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aircraft_type_variants');
    }
}

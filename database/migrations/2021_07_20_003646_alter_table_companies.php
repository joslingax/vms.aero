<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->UnsignedbigInteger('category_id')->nullable();
            $table->UnsignedbigInteger('other_category_id')->nullable();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
            $table->foreign('other_category_id')->references('id')->on('other_categories')->onDelete('set null');

            $table->dropColumn('is_airline');
            $table->dropColumn('is_low_cost');
            $table->dropColumn('is_acmi_charter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropForeign(['other_category_id']);
            $table->dropForeign(['category_id']);

            $table->dropColumn('category_id');
            $table->dropColumn('other_category_id');

            $table->boolean('is_airline')->default(false);
            $table->boolean('is_low_cost')->default(false);
            $table->boolean('is_acmi_charter')->default(false);

        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCountryToManufacturers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aircraft_manufacturers', function (Blueprint $table) {
            
            $table->UnsignedbigInteger('country_id')->nullable()->after("name");
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
        });

        Schema::table('engine_manufacturers', function (Blueprint $table) {
            
            $table->UnsignedbigInteger('country_id')->nullable()->after("name");
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aircraft_manufacturers', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
            $table->dropColumn("country_id");
        });

        Schema::table('engine_manufacturers', function (Blueprint $table) {
            $table->dropForeign(['country_id']);

            $table->dropColumn("country_id");
        });
    }
}

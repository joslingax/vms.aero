<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFleetTableOperator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {

            $table->boolean('is_airline')->default(false);
            $table->boolean('is_low_cost')->default(false);
            $table->boolean('air_ambulance')->default(false);
            $table->boolean('corporate_vip')->default(false);
            $table->boolean('rotary_wings')->default(false);
            $table->boolean('cargo')->default(false);
            $table->boolean('is_acmi_charter')->default(false);
            $table->dropForeign(['other_category_id']);
            $table->dropForeign(['category_id']);
            $table->dropColumn('category_id');
            $table->dropColumn('other_category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {

           
            $table->UnsignedbigInteger('other_category_id')->nullable();

            $table->UnsignedbigInteger('category_id')->nullable();


            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
            $table->foreign('other_category_id')->references('id')->on('other_categories')->onDelete('set null');


            $table->dropColumn('is_airline');
            $table->dropColumn('is_low_cost');
            $table->dropColumn('air_ambulance');
            $table->dropColumn('corporate_vip');
            $table->dropColumn('rotary_wings');
            $table->dropColumn('cargo');
            $table->dropColumn('is_acmi_charter');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFleetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('serial_number')->nullable()->index();
            $table->string('registration')->nullable()->index();
            $table->string('year_of_manufacture',4)->nullable();
            $table->UnsignedbigInteger('company_id');

            $table->UnsignedbigInteger('engine_id')->nullable();
            $table->UnsignedbigInteger('engine_type_variant_id')->nullable();
            $table->UnsignedbigInteger('engine_type_variant_ext_id')->nullable();

            $table->UnsignedbigInteger('aircraft_id')->nullable();
            $table->UnsignedbigInteger('aircraft_type_variant_id')->nullable();
            $table->UnsignedbigInteger('aircraft_type_variant_ext_id')->nullable();
            $table->timestamps();

            $table->foreign('aircraft_id')->references('id')->on('aircrafts')->onDelete('set null');
            $table->foreign('aircraft_type_variant_id')->references('id')->on('aircraft_type_variants')->onDelete('set null');
            $table->foreign('aircraft_type_variant_ext_id')->references('id')->on('aircraft_type_variant_exts')->onDelete('set null');

            
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('engine_id')->references('id')->on('engines')->onDelete('set null');
            $table->foreign('engine_type_variant_id')->references('id')->on('engine_type_variants')->onDelete('set null');
            $table->foreign('engine_type_variant_ext_id')->references('id')->on('engine_type_variant_exts')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets');
    }
}

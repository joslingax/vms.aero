<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAircraftTypeVariantExtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aircraft_type_variant_exts', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('label');
           //$table->UnsignedbigInteger('aircraft_type_variant_id')->nullable();

            $table->timestamps();
            //$table->foreign('aircraft_type_variant_id')->references('id')->on('aircraft_type_variants')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aircraft_type_variant_exts');
    }
}

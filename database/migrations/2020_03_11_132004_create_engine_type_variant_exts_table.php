<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEngineTypeVariantExtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engine_type_variant_exts', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('label');

            //$table->UnsignedbigInteger('engine_type_variant_id')->nullable();

            $table->timestamps();
            //$table->foreign('engine_type_variant_id')->references('id')->on('engine_type_variants')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('engine_type_variant_exts');
    }
}

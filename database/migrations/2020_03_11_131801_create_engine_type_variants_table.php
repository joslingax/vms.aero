<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEngineTypeVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engine_type_variants', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('label')->unique();
           //$table->UnsignedbigInteger('engine_id')->nullable();

            $table->timestamps();
            //$table->foreign('engine_id')->references('id')->on('engines')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('engine_type_variants');
    }
}

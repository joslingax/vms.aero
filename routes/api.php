<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('dashboard', 'API\DashboardController@index');
Route::middleware('auth:api')->get('dispatcher/options/{page}', 'API\OptionDispatcherController@get');
Route::middleware('auth:api')->get('companies/{id}/contacts', 'API\CompanyProfileController@contacts');
Route::middleware('auth:api')->get('companies/{id}/fleets', 'API\CompanyProfileController@fleets');
Route::middleware('auth:api')->get('recherche', 'API\SearchController@search');
Route::middleware('auth:api')->post('search', 'API\SearchController@getR');


Route::group(['middleware' => 'auth:api'], function()
{
    Route::apiResources(
        [
            'users'=>"API\UserController",
            'countries'=>"API\CountryController",
            'contacts'=>"API\ContactController",
            'fleets'=>"API\FleetController",
            'aircrafts'=>"API\AircraftController",
            'companies'=>"API\CompanyController",
            "aircraft_manufacturers"=>"API\AircraftManufacturerController",
            "aircraft_variants"=>"API\AircraftTypeVariantController",
            "aircraft_extensions"=>"API\AircraftTypeVariantExtController",
            "engine_manufacturers"=>"API\EngineManufacturerController",
            'engines'=>"API\EngineController",
            "engine_manufacturers"=>"API\EngineManufacturerController",
            "engine_variants"=>"API\EngineTypeVariantController",
            "engine_extensions"=>"API\EngineTypeVariantExtController",


        ]
    );
});
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('auth')->get('admin/companies/export/', 'API\CompanyController@export');
Route::middleware('auth')->get('admin/fleets/export/', 'API\FleetController@export');
Route::middleware('auth')->get('admin/contacts/export/', 'API\ContactController@export');
Route::get('/', 'GuestController@index');

Route::group(['prefix' => 'admin'], function () {
    
    Route::get('/', 'HomeController@index');
    Auth::routes(['register' => false]);
    Route::get('/{any}', 'HomeController@index')->where('any', '.*');

    

});

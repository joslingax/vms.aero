<?php


namespace App\Exports;

use App\Fleet;
use App\Location;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class FleetExport implements WithEvents, WithTitle, FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize
{          

    use Exportable;
    private $search,$company;
    
    public function title(): string
    {
        return 'fleets';
    }
    public function __construct($search,$company)
    {
        $this->search =$search;
        $this->company =$company;
    }
    //use Exportable; 
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return Fleet::query()->search($this->search)->where('company_id',$this->company);
    }

    public function map($fleet): array
    {
        return [
            $fleet->registration,
            $fleet->serial_number,
            $fleet->year_of_manufacture,
            $fleet->company ? $fleet->company->name :"-",
            $fleet->aircraft ? $fleet->aircraftManufacturer()." ".$fleet->aircraft->type :"-",
            $fleet->aircraftTypeVariant ? $fleet->aircraftTypeVariant->label :"-",
            $fleet->aircraftTypeVariantExt ? $fleet->aircraftTypeVariantExt->label :"-",
            $fleet->engine ? $fleet->engineManufacturer()." ".$fleet->engine->type :"-",
            $fleet->engineTypeVariant ? $fleet->engineTypeVariant->label :"-",
            $fleet->engineTypeVariantExt ? $fleet->engineTypeVariantExt->label :"-",
            

        ];
    }

    public function headings(): array
    {
        return [
            "Registration",
            "Serial Number",
            'Year of manufacture',
            'Company',
            "Aircraft type",
            "Aircraft Variant",
            "Aircraft Ext",
            "Engine_type",
            "Engine Variant",
            "Engine Ext",
        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }

    public function registerEvents(): array
    {
        $center = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]];
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_NONE,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
        return [
            AfterSheet::class    => function(AfterSheet $event) use ($styleArray,$center) {
                $cellRange = 'A1:J1'; // All headers
              $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
             // $event->sheet->getDelegate()->getStyle("B1:D1")->applyFromArray($center);

            },
        ];
    }

}

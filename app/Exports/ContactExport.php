<?php


namespace App\Exports;

use App\Contact;
use App\Location;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ContactExport implements WithEvents, WithTitle, FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize
{          

    use Exportable;
    private $search,$company;
    
    public function title(): string
    {
        return 'contacts';
    }
    public function __construct($search,$company)
    {
        $this->search =$search;
        $this->company =$company;
    }
    //use Exportable; 
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return Contact::query()->search($this->search)->where('company_id',$this->company);
    }

    public function map($contact): array
    {
        return [
            
            $contact->title." ".ucfirst($contact->name),
            $contact->position,
            $contact->company ? $contact->company->name :"-",
            $this->phones($contact),
            
            $this->emails($contact),
            $this->faxes($contact),
            

        ];
    }

    public function headings(): array
    {
        return [
            "Name",
            "Position",
            'Company',
            'Phones',
            "Emails",
            "Faxes",
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function phones($contact)
    {
        $line = "";
        foreach($contact->phones as $phone)
        {
            $l = trim($phone->label)." : ".str_replace(" ","",$phone->content);
            $line = $line."  ".$l;
        }
        $line = trim($line);
        return $line!="" ? str_replace("  "," / ",$line): "No phone number available";
    }

    public function faxes($contact)
    {
        $line = "";
        foreach($contact->faxes as $phone)
        {
            $l = trim($phone->label)." : ".str_replace(" ","",$phone->content);
            $line = $line."  ".$l;
        }
        $line = trim($line);
        return $line!="" ? str_replace("  "," / ",$line): "No fax number available";
    }

    public function emails($contact)
    {
        $line = "";
        foreach($contact->emails as $phone)
        {
            $l = trim($phone->label)." : ".str_replace(" ","",$phone->content);
            $line = $line."  ".$l;
        }
        $line = trim($line);
        return $line!="" ? str_replace("  "," / ",$line): "No email available";
    }
    public function registerEvents(): array
    {
        $center = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]];
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_NONE,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
        return [
            AfterSheet::class    => function(AfterSheet $event) use ($styleArray,$center) {
                $cellRange = 'A1:J1'; // All headers
              $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
              $event->sheet->getDelegate()->getStyle("B1:D1")->applyFromArray($center);

            },
        ];
    }

}

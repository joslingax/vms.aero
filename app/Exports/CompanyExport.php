<?php

namespace App\Exports;

use App\Companies;
use App\Company;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Illuminate\Contracts\Queue\ShouldQueue;


class CompanyExport implements FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize,WithTitle,ShouldQueue
{

    use Exportable; 

    private $search,$country,$region;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($search,$country,$region)
    {
        $this->search =$search;
        $this->country =$country;
        $this->region =$region;
    }
    
    public function title(): string
    {
        return 'companies';
    }
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return Company::query()->search($this->search) 
                        ->country($this->country)
                        ->region($this->region);
    }
 
   //use Exportable; 

    public function map($company): array
    {
        return [
            $company->name,
            $company->address,
            $company->website,
            $company->country  ? $company->country->name :"unknown",
            $company->country  ? ($company->country->region ? $company->country->region->region_name :"unknown") :"unknown",
            $company->is_airline ? "yes" : "no",
            $company->is_acmi_charter ? "yes" : "no",
            $company->is_low_cost ? "yes" : "no",
            $company->air_ambulance ? "yes" : "no",
            $company->corporate_vip ? "yes" : "no",
            $company->cargo ? "yes" : "no",
            $company->rotary_wings  ? "yes" : "no",


        ];
    }

    public function headings(): array
    {
        return [
            "Name ",
            "Adresse",
            'Website',
            'Country',
            "Region",
            'Airline',
            "Acmi-charter",
            'Low-cost',
            'Air ambulance',
            "Corporate Vip",
            'Cargo',
            'Rotary wings',

        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }
}
<?php
namespace App\Aspects;

use App\Company;
use App\Fleet;
use Illuminate\Support\Collection;
use Spatie\Searchable\SearchAspect;
use DB;

class CompanySearchAspect extends SearchAspect
{
    public static $searchType = 'companies';

    public function getResults(string $q): Collection
    {
        return Company::query()->with(['country'])
        ->orWhere("companies.name","like","%".$q."%")

        ->get();
    }
}
?>



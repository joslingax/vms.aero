<?php
namespace App\Aspects;

use App\Fleet;
use Illuminate\Support\Collection;
use Spatie\Searchable\SearchAspect;
use DB;

class FleetSearchAspect extends SearchAspect
{
    public static $searchType = 'fleets';

    public function getResults(string $term): Collection
    {
        return Fleet::query()->with(['company','aircraft','engine'])
        ->where("fleets.serial_number","like","%".$term."%")
        ->orWhere("fleets.registration","like","%".$term."%")
        ->orWhere("fleets.year_of_manufacture","like","%".$term."%")
        ->orWhere(DB::raw("CONCAT(aircrafts.type, ' ',aircraft_manufacturers.name)"),"like","%".$term."%" )
        ->orWhere(DB::raw("CONCAT(engines.type, ' ',engine_manufacturers.name)"),"like","%".$term."%" )
        ->orWhere("companies.name","like","%".$term."%")
        ->leftJoin('companies','companies.id','fleets.company_id')
        ->leftJoin('aircrafts','aircrafts.id','fleets.aircraft_id')
        ->leftJoin('engines','engines.id','fleets.engine_id')
        ->leftJoin('aircraft_manufacturers','aircrafts.manufacturer_id','aircraft_manufacturers.id')
        ->leftJoin('engine_manufacturers','engines.manufacturer_id','engine_manufacturers.id')
        ->get();
    }
}
?>



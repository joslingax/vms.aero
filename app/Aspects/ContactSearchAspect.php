<?php
namespace App\Aspects;

use App\Contact;
use App\Fleet;
use Illuminate\Support\Collection;
use Spatie\Searchable\SearchAspect;
use DB;

class ContactSearchAspect extends SearchAspect
{
    public static $searchType = 'contacts';

    public function getResults(string $q): Collection
    {
        return Contact::query()->with(['company','phones','emails','faxes'])
        ->where("contacts.name","like","%".$q."%")
                         ->orWhere("contacts.position","like","%".$q."%")
                         ->orWhereHas('phones', function($query) use ($q) {
                            $query->where("phone_numbers.content","like","%".$q."%");
                        })
                        ->orWhereHas('emails', function($query) use ($q) {
                            $query->where("emails.content","like","%".$q."%");
                        })
                        ->orWhereHas('faxes', function($query) use ($q) {
                            $query->where("faxes.content","like","%".$q."%");
                        })->get();
    }
}
?>



<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class Engine extends Model
{
    use SearchTrait;
    protected $appends = ['label'];

    protected $guarded = [];

    public function getLabelAttribute()
    {
        $name = $this->manufacturer()->first() ? $this->manufacturer()->first()->name : "";
        return trim(ucfirst($name)." ".$this->type);
    }

    public function manufacturer()
    {
        return $this->belongsTo("App\EngineManufacturer","manufacturer_id");
    }

    // public function variants()
    // {
    //     return $this->hasMany("App\EngineTypeVariant");
    // }
    public function scopeSearch($query, $q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where("engines.type","like","%".$q."%")
                     ->orWhere("engine_manufacturers.name","like","%".$q."%")
                     ->leftJoin('engine_manufacturers','engine_manufacturers.id','engines.manufacturer_id');
    }

    // public function extensions()
    // {
    //   return $this->hasManyThrough('App\EngineTypeVariantExt', 'App\EngineTypeVariant');
    // }
}

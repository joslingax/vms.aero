<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class EngineManufacturer extends Model
{
    use SearchTrait;
    protected $guarded = [];

    public function country()
    {
        return $this->belongsTo("App\Country");
    }

    public function engines()
    {
        return $this->hasMany("App\Engine","manufacturer_id");
    }

    public function scopeSearch($query, $q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where("engine_manufacturers.name","like","%".$q."%")
                     ->orWhere("countries.name","like","%".$q."%")
                     ->leftJoin('countries','countries.id','engine_manufacturers.country_id');
    }
}

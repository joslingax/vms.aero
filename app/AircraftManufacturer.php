<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class AircraftManufacturer extends Model
{

    use SearchTrait;
    protected $guarded = [];

    public function country()
    {
        return $this->belongsTo("App\Country");
    }

    public function aircrafts()
    {
        return $this->hasMany("App\Aircraft","manufacturer_id");
    }
    public function scopeSearch($query, $q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where("aircraft_manufacturers.name","like","%".$q."%")
                     ->orWhere("countries.name","like","%".$q."%")
                     ->leftJoin('countries','countries.id','aircraft_manufacturers.country_id');
    }
}

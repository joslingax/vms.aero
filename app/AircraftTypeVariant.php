<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class AircraftTypeVariant extends Model
{
    use SearchTrait;
    
    protected $guarded = [];
    public static $searchables = ['label'];
    // public function aircraft()
    // {
    //     return $this->belongsTo("App\Aircraft");
    // }

    // public function extensions()
    // {
    //     return $this->hasMany("App\AircraftTypeVariantExt");
    // }

    public function scopeSearch($query, $q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where("label","like","%".$q."%");
    }
}

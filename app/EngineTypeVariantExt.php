<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class EngineTypeVariantExt extends Model
{
    use SearchTrait;
    protected $guarded = [];
    public static $searchables = ['label'];
    // public function engineTypeVariant()
    // {
    //     return $this->belongsTo("App\EngineTypeVariant","engine_type_variant_id");
    // }

    public function scopeSearch($query, $q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where("label","like","%".$q."%");
    }
}

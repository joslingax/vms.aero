<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
class Fleet extends Model implements Searchable
{

    use SearchTrait;
    protected $guarded = [];
    protected $appends = ["engineTypeVariant","engineTypeVariantExt","aircraftTypeVariant","aircraftTypeVariantExt"];

    public function getSearchResult(): SearchResult{
        $search = new SearchResult(
            $this,
            $this->serial_number ? $this->serial_number : "Empty fleet"
        );
    $search->setType("fleets");
    return $search;
    }

    public function company()
    {
        return $this->belongsTo("App\Company");
    }

    public function engine()
    {
        return $this->belongsTo("App\Engine");
    }

    public function getEngineTypeVariantAttribute()
    {
        $variant = EngineTypeVariant::whereId($this->engine_type_variant_id)->first();
        return $variant;
    }
    public function getEngineTypeVariantExtAttribute()
    {
        $ext = EngineTypeVariantExt::whereId($this->engine_type_variant_ext_id)->first();
        return $ext;
    }

    public function engineTypeVariantExt()
    {
        return $this->belongsTo("App\EngineTypeVariantExt");
    }
    public function aircraftManufacturer()
    {
        return $this->aircraft ? $this->aircraft->manufacturer->name : "";
    }

    public function engineManufacturer()
    {
        return $this->engine ? $this->engine->manufacturer->name : "";
    }
    public function aircraft()
    {
        return $this->belongsTo("App\Aircraft");
    }
    public function getAircraftTypeVariantAttribute()
    {
        $variant = AircraftTypeVariant::whereId($this->aircraft_type_variant_id)->first();
        return $variant;
    }
    public function getAircraftTypeVariantExtAttribute()
    {
        $ext = AircraftTypeVariantExt::whereId($this->aircraft_type_variant_ext_id)->first();
        return $ext;
    }
    public function aircraftTypeVariant()
    {
        return $this->belongsTo("App\AircraftTypeVariant");
    }

    public function aircraftTypeVariantExt()
    {
        return $this->belongsTo("App\AircraftTypeVariantExt");
    }

    public function scopeSearch($query, $q,$byCompany=false)
    {
        if($q==null  || $q=="")
        {
            return $query;
        }

        elseif($byCompany==false)
        {
            return $query->where("fleets.serial_number","like","%".$q."%")
            ->orWhere("fleets.registration","like","%".$q."%")
            ->orWhere("fleets.year_of_manufacture","like","%".$q."%")
            ->orWhere("aircrafts.type","like","%".$q."%")
            ->orWhere("aircraft_type_variants.label","like","%".$q."%")
            ->orWhere("aircraft_type_variant_exts.label","like","%".$q."%")
            ->orWhere("engines.type","like","%".$q."%")
            ->orWhere("engine_type_variants.label","like","%".$q."%")
            ->orWhere("engine_type_variant_exts.label","like","%".$q."%")
            ->orWhere("companies.name","like","%".$q."%")
            ->leftJoin('companies','companies.id','fleets.company_id')
            ->leftJoin('aircrafts','aircrafts.id','fleets.aircraft_id')
            ->leftJoin('aircraft_type_variants','aircraft_type_variants.id','fleets.aircraft_type_variant_id')
            ->leftJoin('aircraft_type_variant_exts','aircraft_type_variant_exts.id','fleets.aircraft_type_variant_ext_id')
            ->leftJoin('engines','engines.id','fleets.engine_id')
            ->leftJoin('engine_type_variants','engine_type_variants.id','fleets.engine_type_variant_id')
            ->leftJoin('engine_type_variant_exts','engine_type_variant_exts.id','fleets.engine_type_variant_ext_id');

        }

        elseif($byCompany==true)
        {
            return $query->where("fleets.serial_number","like","%".$q."%")
            ->orWhere("fleets.registration","like","%".$q."%")
            ->orWhere("fleets.year_of_manufacture","like","%".$q."%")
            ->orWhere("aircrafts.type","like","%".$q."%")
            ->orWhere("aircraft_type_variants.label","like","%".$q."%")
            ->orWhere("aircraft_type_variant_exts.label","like","%".$q."%")
            ->orWhere("engines.type","like","%".$q."%")
            ->orWhere("engine_type_variants.label","like","%".$q."%")
            ->orWhere("engine_type_variant_exts.label","like","%".$q."%")
            ->leftJoin('aircrafts','aircrafts.id','fleets.aircraft_id')
            ->leftJoin('aircraft_type_variants','aircraft_type_variants.id','fleets.aircraft_type_variant_id')
            ->leftJoin('aircraft_type_variant_exts','aircraft_type_variant_exts.id','fleets.aircraft_type_variant_ext_id')
            ->leftJoin('engines','engines.id','fleets.engine_id')
            ->leftJoin('engine_type_variants','engine_type_variants.id','fleets.engine_type_variant_id')
            ->leftJoin('engine_type_variant_exts','engine_type_variant_exts.id','fleets.engine_type_variant_ext_id');
        }
     }
}

<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class AircraftTypeVariantExt extends Model
{
    use SearchTrait;
    
    protected $guarded = [];
    public static $searchables = ['label'];


    public function scopeSearch($query, $q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where("label","like","%".$q."%");
    }
}

<?php

namespace App;

use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;

class Company extends Model implements Searchable
{
    protected $guarded = [];
        /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'cargo' => 'boolean',
        'air_ambulance' => 'boolean',
        'corporate_vip' => 'boolean',
        'rotary_wings' => 'boolean',
        'is_acmi_charter' => 'boolean',
        'is_airline' => 'boolean',
        'is_low_cost' => 'boolean',
    ];
    
    public function country()
    {
        return $this->belongsTo("App\Country");
    }

    public function contacts()
    {
        return $this->hasMany("App\Contact");
    }
    public function  fleets()
    {
        return $this->hasMany("App\Fleet");
    }

    public function getSearchResult(): SearchResult{
        $search = new SearchResult(
            $this,
            $this->name ? $this->name : "Empty Company"
        );
    $search->setType("companies");
    return $search;
    }
    public function scopeCountry($query,$q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where('companies.country_id',$q);
    }

    public function scopeSearch($query,$q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->orWhere("companies.name","like","%".$q."%")
              ->orWhere("companies.website","like","%".$q."%")
              ->orWhere("companies.address","like","%".$q."%");
    }
    public function scopeRegion($query,$q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where('countries.region_id',$q)
        ->leftJoin('countries','countries.id',"companies.country_id")
        ->leftJoin('regions','regions.id',"countries.region_id");
    }

    public function scopeLowcost($query,$q)
    {
        if($q==null  || $q=="" || $q=="all")
        return $query;
        else
        {
            $q = mb_strtolower($q);
            $q = ($q==false)? 0 : 1 ;

            return $query->where('companies.is_low_cost',$q);
        }

    }

    public function scopeCargo($query,$q)
    {
        return $query;
        if($q==null  || $q=="" || $q=="all")
        return $query;
        else
        {
            $q = mb_strtolower($q);
            $q = ($q==false)? 0 : 1 ;

            return $query->where('companies.cargo',$q);
        }

    }
    public function scopeCorporateVip($query,$q)
    {
        return $query;
        if($q==null  || $q=="" || $q=="all")
        return $query;
        else
        {
            $q = mb_strtolower($q);
            $q = ($q==false)? 0 : 1 ;

            return $query->where('companies.corporate_vip',$q);
        }

    }
    public function scopeRotaryWings($query,$q)
    {
        return $query;
        if($q==null  || $q=="" || $q=="all")
        return $query;
        else
        {
            $q = mb_strtolower($q);
            $q = ($q==false)? 0 : 1 ;

            return $query->where('companies.rotary_wings',$q);
        }

    }

    public function scopeAirAmbulance($query,$q)
    {
        return $query;
        if($q==null  || $q=="" || $q=="all")
        return $query;
        else
        {
            $q = mb_strtolower($q);
            $q = ($q==false)? 0 : 1 ;

            return $query->where('companies.air_ambulance',$q);
        }

    }


    public function scopeEngineType($query, $q) {
        if($q==null  || $q=="" ) return $query;
        return $query->whereHas('fleets', function($query) use ($q) {
            $query->where('fleets.engine_id',$q);
        });
     }
     public function scopeEngineVariant($query, $q) {
        if($q==null  || $q=="" ) return $query;
        return $query->whereHas('fleets', function($query) use ($q) {
            $query->where('fleets.engine_type_variant_id',$q);
        });
     }
     public function scopeEngineExt($query, $q) {
        if($q==null  || $q=="" ) return $query;
        return $query->whereHas('fleets', function($query) use ($q) {
            $query->where('fleets.engine_type_variant_ext_id',$q);
        });
     }
    public function scopeAircraftType($query, $q) {
        if($q==null  || $q=="" ) return $query;
        return $query->whereHas('fleets', function($query) use ($q) {
            $query->where('fleets.aircraft_id',$q);
        });
     }
     public function scopeAircraftVariant($query, $q) {
        if($q==null  || $q=="" ) return $query;
        return $query->whereHas('fleets', function($query) use ($q) {
            $query->where('fleets.aircraft_type_variant_id',$q);
        });
     }
     public function scopeAircraftExt($query, $q) {
        if($q==null  || $q=="" ) return $query;
        return $query->whereHas('fleets', function($query) use ($q) {
            $query->where('fleets.aircraft_type_variant_ext_id',$q);
        });
     }
    public function scopeCategory($query,$q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where('companies.category_id',$q);

    }
    public function scopeOther($query,$q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where('companies.other_category_id',$q);

    }
    public function scopeCharter($query,$q)
    {
        if($q==null  || $q=="" || $q=="all")
        return $query;
        else
        {
            $q = mb_strtolower($q);
            $q = ($q=="false")? 0 : 1 ;

            return $query->where('companies.is_acmi_charter',$q);
        }

    }

    public function scopeAirline($query,$q)
    {

        if($q==null  || $q=="" || $q=="all")
        return $query;
        else
        {
            $q = mb_strtolower($q);
            $q = ($q=="false")? 0 : 1 ;
            return $query->where('companies.is_airline',$q);
        }

    }
}

<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Country extends Model
{
    use SearchTrait;
    
    protected $guarded = [];
    protected $appends = ['hasFlag'];

    
    public function region()
    {
        return $this->belongsTo("App\Region");
    }

    public function getHasFlagAttribute()
    {
        return file_exists(storage_path("/app/public/flags/".$this->code.'.png')) ? true : false;
    }

    public function scopeRegion($query,$q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where('region_id',$q);
    }

    public function scopeSearch($query,$q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where("name","like","%".$q."%");
    }
}

<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model
{
    use SearchTrait;
    
    protected $guarded = [];

    public function contact()
    {
        return $this->belongsTo("App\Contact");
    }
}

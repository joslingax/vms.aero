<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aircraft extends Model
{
    protected $guarded = [];
    protected $appends = ['label'];
    
    public function getLabelAttribute()
    {
        $name = $this->manufacturer()->first() ? $this->manufacturer()->first()->name : "";
        return trim(ucfirst($name)." ".$this->type);
    }
    public function manufacturer()
    {
        return $this->belongsTo("App\AircraftManufacturer","manufacturer_id");
    }

    // public function variants()
    // {
    //     return $this->hasMany("App\AircraftTypeVariant");
    // }

    // public function extensions()
    // {
    //   return $this->hasManyThrough('App\AircraftTypeVariantExt', 'App\AircraftTypeVariant');
    // }



    public function scopeSearch($query, $q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where("aircrafts.type","like","%".$q."%")
                     ->orWhere("aircraft_manufacturers.name","like","%".$q."%")
                     ->leftJoin('aircraft_manufacturers','aircraft_manufacturers.id','aircrafts.manufacturer_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use Spatie\Searchable\SearchAspect;

class Contact extends Model implements Searchable
{

    protected $guarded = [];

    
    //Méthodes propres
    public function getSearchResult(): SearchResult{
        $search = new SearchResult(
            $this,
            $this->name ? $this->name : "Empty Contact"
        );
    $search->setType("contacts");
    return $search;
    }
    public function addPhones($phones=[])
    {
        $phones = collect($phones);
       
        $phones->each(function ($phone, $key) {
              if($phone["content"]!=null && $phone["content"]!="")
              $this->phones()->create(
                  [
                    "label"=>($phone["label"] =="" || $phone["label"]==null) ? "phone ".($key+1) : $phone["label"],
                    "content"=>$phone["content"],
                    "contact_id"=>$this->id,
                    "is_main"=>$phone["is_main"]
                  ]);
         });

    }
    public function addFaxes($faxes=[])
    {
        $faxes = collect($faxes);
        $faxes->each(function ($fax, $key) {
             if($fax["content"]!=null && $fax["content"]!="")
              $this->faxes()->create(
                  [
                    "label"=>($fax["label"] =="" || $fax["label"]==null) ? "fax ".($key+1) : "".($fax["label"]),
                    "content"=>$fax["content"],
                    "contact_id"=>$this->id,
                    "is_main"=>$fax["is_main"]
                  ]);
         });

    }
    public function addEmails($emails=[])
    {
        $emails = collect($emails);
        $emails->each(function ($email, $key) {
            if($email["content"]!=null && $email["content"]!="")
              $this->emails()->create(
                  [
                    "label"=>($email["label"] =="" || $email["label"]==null) ? "email ".($key+1) : "".($email["label"]),
                    "content"=>$email["content"],
                    "contact_id"=>$this->id,
                    "is_main"=>$email["is_main"]
                  ]);
         });

    }
    //Relations
    public function company()
    {
        return $this->belongsTo("App\Company");
    }

    public function phones()
    {
        return $this->hasMany("App\PhoneNumber");
    }

    public function emails()
    {
        return $this->hasMany("App\Email");
    }

    public function faxes()
    {
        return $this->hasMany("App\Fax");
    }
    
    public function scopeSearch($query,$q,$byCompany=false)
    {


        if($q==null  || $q=="")
        {
            return $query;
        }

        elseif($byCompany==false)
        {
            return $query->where("contacts.name","like","%".$q."%")
            ->orWhere("contacts.position","like","%".$q."%")
            ->orWhere("companies.name","like","%".$q."%")
            ->whereHas('phones', function($query) use ($q) {
                $query->where("phone_numbers.content","like","%".$q."%");
            })
            ->orWhereHas('emails', function($query) use ($q) {
                $query->where("emails.content","like","%".$q."%");
            })
            ->orWhereHas('faxes', function($query) use ($q) {
                $query->where("faxes.content","like","%".$q."%");
            })
            ->leftJoin('companies','companies.id','contacts.company_id');
        }

        elseif($byCompany==true)
        {
           // dd($q,$byCompany);

            return $query->where("contacts.name","like","%".$q."%")
                         ->orWhere("contacts.position","like","%".$q."%")
                         ->orWhereHas('phones', function($query) use ($q) {
                            $query->where("phone_numbers.content","like","%".$q."%");
                        })
                        ->orWhereHas('emails', function($query) use ($q) {
                            $query->where("emails.content","like","%".$q."%");
                        })
                        ->orWhereHas('faxes', function($query) use ($q) {
                            $query->where("faxes.content","like","%".$q."%");
                        });
        }

    }
}

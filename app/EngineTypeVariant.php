<?php

namespace App;

use App\Traits\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class EngineTypeVariant extends Model
{
    use SearchTrait;
    protected $guarded = [];
    public static $searchables = ['label'];
    // public function engine()
    // {
    //     return $this->belongsTo("App\Engine");
    // }

    // public function variants()
    // {
    //     return $this->hasMany("App\AircraftTypeVariant");
    // }
    public function scopeSearch($query, $q)
    {
        if($q==null  || $q=="")
        return $query;
        else
        return $query->where("label","like","%".$q."%");
    }
}

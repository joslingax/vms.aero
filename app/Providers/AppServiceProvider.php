<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema as FacadesSchema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
                
        if (App::environment('local')) 
        {
            // The environment is local
        }
        else
        {
            \Illuminate\Support\Facades\URL::forceScheme('https');
        }
        FacadesSchema::defaultStringLength(191);
    }
}

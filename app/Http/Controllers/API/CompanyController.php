<?php

namespace App\Http\Controllers\API;

use App\Company;
use App\Exports\CompanyExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Jobs\NotifyUserOfCompletedExport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');

        $at= request()->query('filter_aircraft_type') == null ? null : request()->query('filter_aircraft_type');
        $av= request()->query('filter_aircraft_variant') == null ? null : request()->query('filter_aircraft_variant');
        $ev= request()->query('filter_engine_variant') == null ? null : request()->query('filter_engine_variant');
        $et= request()->query('filter_engine_type') == null ? null : request()->query('filter_engine_type');

        $r = request()->query('filter_region') == null ? null : request()->query('filter_region');
        $c = request()->query('filter_country') == null ? null : request()->query('filter_country');
        $category_id = request()->query('filter_category_id') == null ? null : request()->query('filter_category_id');
        $other_category_id = request()->query('filter_other_category_id') == null ? null : request()->query('filter_other_category_id');
        $is_airline = request()->query('filter_airline') == null ? null : request()->query('filter_airline');
        $is_lowcost = request()->query('filter_low_cost') == null ? null : request()->query('filter_low_cost');
        $is_charter = request()->query('filter_charter') == null ? null : request()->query('filter_charter');
        $cargo = request()->query('filter_cargo') == null ? null : request()->query('filter_cargo');
        $corporate_vip = request()->query('filter_corporate_vip') == null ? null : request()->query('filter_corporate_vip');
        $rotary_wings = request()->query('filter_rotary_wings') == null ? null : request()->query('filter_rotary_wings');
        $air_ambulance = request()->query('filter_air_ambulance') == null ? null : request()->query('filter_air_ambulance');

        return    Company::select('companies.*')->with(['country','contacts', 'fleets'])->search($q)
                                             ->country($c)
                                             ->region($r)
                                             ->aircraftType($at)
                                             ->aircraftVariant($av)
                                             ->engineType($et)
                                             ->engineVariant($ev)
                                             ->airline($is_airline)
                                             ->lowcost($is_lowcost)
                                             ->charter($is_charter)
                                             ->airAmbulance($air_ambulance)
                                             ->cargo($cargo)
                                             ->rotaryWings($rotary_wings)
                                             ->corporateVip($corporate_vip)
                                          
                                             ->orderBy("companies.name",'asc')
                                            ->paginate($per);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        Company::create([
            'country_id' => $request->input("country_id"),
            'website' => $request->input("website"),
            'address' => $request->input("address"),
            'name' => $request->input("name"),
            'is_airline' => $request->input("is_airline"),
            'is_low_cost' => $request->input("is_low_cost"),
            'is_acmi_charter' => $request->input("is_acmi_charter"),
            'cargo' => $request->input("cargo"),
            'corporate_vip' => $request->input("corporate_vip"),
            'rotary_wings' => $request->input("rotary_wings"),
            'air_ambulance' => $request->input("air_ambulance"),


        ]);

        return response()->json(["sucess"=>true],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $Company
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::whereId($id)->first();
        return  [ "company" => $company->load('country')] ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {
        Company::whereId($id)->update([
            'country_id' => $request->input("country_id"),
            'website' => $request->input("website"),
            'address' => $request->input("address"),
            'name' => $request->input("name"),
            'is_airline' => $request->input("is_airline"),
            'is_low_cost' => $request->input("is_low_cost"),
            'is_acmi_charter' => $request->input("is_acmi_charter"),
            'cargo' => $request->input("cargo"),
            'corporate_vip' => $request->input("corporate_vip"),
            'rotary_wings' => $request->input("rotary_wings"),
            'air_ambulance' => $request->input("air_ambulance"),
        ]);

        $company =Company::with(['country'])->whereId($id)->first();

        return response()->json(["sucess"=>true,"company"=>$company],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::whereId($id)->first();
        if($company)
        {
            $company->delete();
        }
    }

    public function export() 
    {
        //dd(request()->user());
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $country = request()->query('filter_country') == null ? null : request()->query('filter_country');
        $region = request()->query('filter_region') == null ? null : request()->query('filter_region');
        $now = Carbon::now()->toDateTimeString();
        $now =str_replace(" ","_",$now);
        $now =str_replace("-","_",$now);
        $now =str_replace(":","_",$now);
        $filename = "companies_".$now.'.xlsx';

       return (new  CompanyExport($q,$country,$region))->download($filename);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\AircraftManufacturer;
use App\Http\Controllers\Controller;
use App\Http\Requests\AircraftManufacturerRequest;
use Illuminate\Http\Request;

class AircraftManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        return  AircraftManufacturer::select('aircraft_manufacturers.id','aircraft_manufacturers.created_at','aircraft_manufacturers.name','aircraft_manufacturers.country_id')
        ->with(['country'])->search($q)->orderBy("aircraft_manufacturers.created_at",'desc')->paginate($per);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AircraftManufacturerRequest $request)
    {

        $validatedData = $request->validate([
            'name' => 'required',
            'country_id' => 'required|exists:countries,id',
        ]);

        AircraftManufacturer::create(
            [
                "name" => $request->input('name'),
                "country_id" => $request->input('country_id')
            ]);
        
      return response()->json(["success"=>true],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AircraftManufacturerRequest $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'country_id' => 'required|exists:countries,id',
        ]);
        AircraftManufacturer::whereId($id)->update(
            [
                "name" => $request->input('name'),
                "country_id" => $request->input('country_id')
            ]);
        
      return response()->json(["success"=>true],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AircraftManufacturer::whereId($id)->first()->delete();
    }
}

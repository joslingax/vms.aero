<?php

namespace App\Http\Controllers\API;

use App\Exports\FleetExport;
use App\Fleet;
use App\Http\Controllers\Controller;
use App\Http\Requests\FleetRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FleetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        return    Fleet::with(['company',"engine","aircraft",'engineTypeVariant','engineTypeVariantExt'])->search($q)->orderBy("fleets.created_at",'desc')->paginate($per);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FleetRequest $request)
    {
        Fleet::create([
            'serial_number' =>$request->input("serial_number"),
            'registration' => $request->input("registration"),
            'year_of_manufacture' => $request->input("year_of_manufacture"),
            'configuration' =>$request->input("configuration"),

            'company_id' =>$request->input("company_id"),

            'engine_id' =>$request->input("engine_id"),
            'engine_type_variant_id' =>$request->input("engine_type_variant_id"),
            'engine_type_variant_ext_id' =>$request->input("engine_type_variant_ext_id"),

            'aircraft_id' =>$request->input("aircraft_id"),
            'aircraft_type_variant_id' =>$request->input("aircraft_type_variant_id"),
            'aircraft_type_variant_ext_id' =>$request->input("aircraft_type_variant_ext_id"),
        ]);

        return response()->json(["sucess"=>true],201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FleetRequest $request, $id)
    {
        Fleet::whereId($id)->update([
            'serial_number' =>$request->input("serial_number"),
            'registration' => $request->input("registration"),
            'year_of_manufacture' => $request->input("year_of_manufacture"),
            
            'company_id' =>$request->input("company_id"),
            'configuration' =>$request->input("configuration"),

            'engine_id' =>$request->input("engine_id"),
            'engine_type_variant_id' =>$request->input("engine_type_variant_id"),
            'engine_type_variant_ext_id' =>$request->input("engine_type_variant_ext_id"),

            'aircraft_id' =>$request->input("aircraft_id"),
            'aircraft_type_variant_id' =>$request->input("aircraft_type_variant_id"),
            'aircraft_type_variant_ext_id' =>$request->input("aircraft_type_variant_ext_id"),
        ]);

        return response()->json(["sucess"=>true,'fleet'=>Fleet::with(['company',"engine","aircraft"])->whereId($id)->first()],200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Fleet::whereId($id)->first()->delete();
    }

    public function export() 
    {
        //dd(request()->user());
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $company= request()->query('company') == null ? null : request()->query('company');

        $now = Carbon::now()->toDateTimeString();
        $now =str_replace(" ","_",$now);
        $now =str_replace("-","_",$now);
        $now =str_replace(":","_",$now);
        $filename = "fleets_".$now.'.xlsx';

       return (new FleetExport($q,$company))->download($filename);
    }
}

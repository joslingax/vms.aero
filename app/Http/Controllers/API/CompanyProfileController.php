<?php

namespace App\Http\Controllers\API;

use App\Contact;
use App\Fleet;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompanyProfileController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contacts($id)
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $company= $id;

        return    Contact::with(['company','emails',"faxes",'phones'])->where('company_id',$company)->search($q,true)->orderBy("contacts.created_at",'desc')->paginate($per);

    }

    public function fleets($id)
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $company= $id;
        return    Fleet::with(['aircraft','engine'])->where('company_id',$company)->search($q,true)->orderBy("fleets.created_at",'desc')->paginate($per);

    }
}

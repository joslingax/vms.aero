<?php

namespace App\Http\Controllers\API;

use App\EngineTypeVariant;
use App\Http\Controllers\Controller;
use App\Http\Requests\EngineTypeVariantRequest;
use Illuminate\Http\Request;

class EngineTypeVariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        return    EngineTypeVariant::search($q)->orderBy("created_at",'desc')->paginate($per);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EngineTypeVariantRequest $request)
    {
        EngineTypeVariant::create([
            'label' => $request->input('label'),
           // 'engine_id' => $request->input('engine_id'),
        ]);

        return response()->json(['status' =>true],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EngineTypeVariantRequest $request, $id)
    {
        EngineTypeVariant::whereId($id)->update([
            'label' => $request->input('label'),
            //'engine_id' => $request->input('engine_id'),
        ]);

        return response()->json(['status' =>true],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EngineTypeVariant::whereId($id)->first()->delete();

    }
}

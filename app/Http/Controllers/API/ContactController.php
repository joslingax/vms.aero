<?php

namespace App\Http\Controllers\API;

use App\Contact;
use App\Exports\ContactExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        return    Contact::with(['company','emails',"faxes",'phones'])->search($q)->orderBy("contacts.created_at",'desc')->paginate($per);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {

        try
        {
            DB::beginTransaction();

            $contact = Contact::create([
                'company_id' => $request->input("company_id"),
                'name' => $request->input("name"),
                'position' => $request->input("position"),
                'title' => $request->input("title"),
            ]);
            
            $contact->addPhones([$request->input('phones')][0]);
            $contact->addEmails([$request->input('emails')][0]);
            $contact->addFaxes([$request->input('faxes')][0]);

            DB::commit();

            return response()->json(["sucess"=>true],201);

        }catch(\Exception $e)
        {
            return response()->json(["sucess"=>false,"e"=>$e->getMessage()],201);
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContactRequest $request, $id)
    {



        try
        {
            DB::beginTransaction();

            $contact = Contact::whereId($id)->first();
            $contact->company_id = $request->input("company_id");
            $contact->name = $request->input("name");
            $contact->position = $request->input("position");
            $contact->title = $request->input("title");
            $contact->save();

            $contact->phones()->delete();
            $contact->emails()->delete();
            $contact->faxes()->delete();

            $contact->addPhones([$request->input('phones')][0]);
            $contact->addEmails([$request->input('emails')][0]);
            $contact->addFaxes([$request->input('faxes')][0]);

            DB::commit();

            return response()->json(["sucess"=>true,"contact"=>$contact->fresh()->load(['company','phones','emails','faxes'])],201);

        }catch(\Exception $e)
        {
            return response()->json(["sucess"=>false,"e"=>$e->getMessage()],201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::whereId($id)->first();
        if($contact)
        {
            $contact->delete();
        }
        
    }

    public function export() 
    {
        //dd(request()->user());
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $company= request()->query('company') == null ? null : request()->query('company');

        $now = Carbon::now()->toDateTimeString();
        $now =str_replace(" ","_",$now);
        $now =str_replace("-","_",$now);
        $now =str_replace(":","_",$now);
        $filename = "contacts_".$now.'.xlsx';

       return (new ContactExport($q,$company))->download($filename);
    }
}

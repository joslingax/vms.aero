<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;


class DashboardController extends Controller
{
 
    public function index()
    {
        $fleets_count= DB::table('fleets')->count();
        $contacts_count =  DB::table('contacts')->count();
        $companies_count = DB::table('companies')->count();
        $users_count = DB::table('users')->count();

        return [
            "fleets" => [
                "count"=>$fleets_count,
                "wording"=>Str::plural('Fleet',$fleets_count==0? 1 : $fleets_count)
            ],
            "contacts" => [
                "count"=>$contacts_count,
                "wording"=>Str::plural('Contact',$contacts_count == 0 ? 1 :$contacts_count)
            ],
            "companies" => [
                "count"=>$companies_count,
                "wording"=>Str::plural('Company',$companies_count== 0 ? 1 :$companies_count)
            ],
            "users" => [
                "count"=>$users_count,
                "wording"=>Str::plural('User',$users_count== 0 ? 1 :$users_count)
            ],   
        ];
    }

}

<?php

namespace App\Http\Controllers\Api;

use App\Aircraft;
use App\AircraftManufacturer;
use App\AircraftTypeVariant;
use App\AircraftTypeVariantExt;
use App\Category;
use App\Company;
use App\Country;
use App\Engine;
use App\EngineManufacturer;
use App\EngineTypeVariant;
use App\EngineTypeVariantExt;
use App\HorsCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OtherCategory;
use App\Region;


class OptionDispatcherController extends Controller
{

    public function get(Request $request,  $page)
    {
        //options pour la page vehicule
        if($page =="contacts")
        {
            return
            [
                "companies"=>Company::orderBy("name",'asc')->get()
            ];
        }
        if($page =="countries")
        {
            return
            [
                "regions"=>Region::orderBy("region_name",'asc')->get()
            ];
        }
        if($page =="engine_type_variant" || $page =="engines" )
        {
            return
            [
                "manufacturers"=>EngineManufacturer::with(['engines'])->orderBy("created_at",'asc')->get()
            ];
        }
        if($page =="engine_type_extension" )
        {
            return
            [
                "manufacturers"=>EngineManufacturer::orderBy("created_at",'asc')->get()
            ];
        }
        if($page =="aircraft_type_variant" || $page =="aircrafts" )
        {
            return
            [
                "manufacturers"=>AircraftManufacturer::with(['aircrafts'])->orderBy("created_at",'asc')->get()
            ];
        }
        if($page =="aircraft_type_extension" )
        {
            return
            [
                "manufacturers"=>AircraftManufacturer::orderBy("created_at",'asc')->get()
            ];
        }
        if($page =="companies" || $page =="aircraft_manufacturers" || $page =="engine_manufacturers")
        {
            return
            [
                "categories"=>Category::orderBy("label",'asc')->get(),
                "othercategories"=>OtherCategory::orderBy("label",'asc')->get(),

                "countries"=>Country::with(['region'])->orderBy("name",'asc')->get(),
                "aircrafts"=>Aircraft::with(['manufacturer'])->orderBy("type",'asc')->get(),
                "engines"=>Engine::with(['manufacturer'])->orderBy("type",'asc')->get(),
                "engine_variants"=>EngineTypeVariant::orderBy("label",'asc')->get(),
                "aircraft_variants"=>AircraftTypeVariant::orderBy("label",'asc')->get(),
                "regions"=>Region::orderBy("region_name",'asc')->get()
            ];
        }
        if($page =="fleets")
        {
            return
            [
                "companies"=>Company::orderBy("name",'asc')->get(),
                "aircrafts"=>Aircraft::with(['manufacturer'])->orderBy("created_at",'asc')->get(),
                "engines"=>Engine::with(['manufacturer'])->orderBy("created_at",'asc')->get(),
                "e_variants"=>EngineTypeVariant::orderBy("created_at",'asc')->get(),
                "e_exts"=>EngineTypeVariantExt::orderBy("created_at",'asc')->get(),

                "a_variants"=>AircraftTypeVariant::orderBy("created_at",'asc')->get(),
                "a_exts"=>AircraftTypeVariantExt::orderBy("created_at",'asc')->get(),
            ];
        }
    }

}

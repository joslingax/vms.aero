<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;


class UserController extends Controller
{

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $role = request()->query('filter_role') == null ? null : request()->query('filter_role') ;

        return  [
                 "users" => User::search($q)->filterRole($role)->orderBy("users.created_at",'desc')->paginate($per),
                 "roles" =>['admin',"reader","basic"]
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $token = Str::random(80);
        
       $user = User::create([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(25),
            'api_token'=> $token,
            'role'=>$request->input('role')

        ]);
        return response()->json(['success' => true],201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::whereId($id)->first();
        if($user)
        {
            return response()->json(['success' => true,'user'=>$user->load(['role'])],200);
        }
        else
        {
            return response()->json(['success' => false],200);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        //on recupere l(utilisateur)
        $user = User::find($id);
        $user->name=$request->input('name');
        $user->username=$request->input('username');
        $user->email =$request->input('email');
        $user->role=$request->input('role');
        $user->save();

    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $user = User::whereId($id)->first();
        if($user)
        {
            $user->delete();
            return response()->json([
                'message' => ' Supprimé avec succès']);
        }
        else
        {
            return response()->json([
            'message' => 'Action impossible']);
        }

    }

    
    public function export() 
    {

        // $q = request()->query('filter') == null ? null : request()->query('filter');
        // $d_fin = request()->query('filter_date_fin') == null ? null : request()->query('filter_date_fin');
        // $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');
        // $annee= request()->query('annee') == null ? null : request()->query('annee');
        // $mois = request()->query('mois') == null ? null : request()->query('mois');
        // $vehicule = request()->query('vehicule_id') == null ? null : request()->query('vehicule_id');

        $q = request()->query('filter') == null ? null : request()->query('filter');
        $format = request()->query('format') == null ? null : request()->query('format');
        $role = request()->query('filter_role') == null ? null : request()->query('filter_role');
        $filename = "utilisateurs-".Carbon::now()->toDateTimeString();
        if($format)
        {
           if($format=="excel")
           return (new UserExport($q,$role))->download($filename.'.xlsx');
        }
        
    }
}

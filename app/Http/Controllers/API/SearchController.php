<?php

namespace App\Http\Controllers\API;

use App\Aspects\CompanySearchAspect;
use App\Aspects\ContactSearchAspect;
use App\Aspects\FleetSearchAspect;
use App\Company;
use App\Contact;
use App\Fleet;
use App\Marque;
use App\Http\Controllers\Controller;
use App\Location;
use App\Vehicule;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;
use Spatie\Searchable\ModelSearchAspect;

class SearchController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
       return $request->input('q') != null ? $searchResults = (new Search())
            ->registerAspect(CompanySearchAspect::class)
            ->registerAspect(ContactSearchAspect::class)
            ->registerAspect(FleetSearchAspect::class)
            ->perform(strtoupper($request->query('q')))->take(8): [];

        
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getR (Request $request)
    {
       $result = $request->input('search') != null ? $searchResults = (new Search())
           ->registerAspect(CompanySearchAspect::class)
           ->registerAspect(ContactSearchAspect::class)
           ->registerAspect(FleetSearchAspect::class)
           ->search($request->input('search')) : collect([]);

        return 
        [
            'res' =>$result,
            'companies' =>($result->filter(function($obj){ return $obj->type=="companies";}))->all(),
            'contacts' =>($result->filter(function($o){ return $o->type=="contacts";}))->all(),
            'fleets' =>($result->filter(function($ob){ return $ob->type=="fleets";}))->all(),

        ];
                 

        
    }

}

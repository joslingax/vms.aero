<?php

namespace App\Http\Controllers\API;

use App\EngineTypeVariantExt;
use App\Http\Controllers\Controller;
use App\Http\Requests\EngineTypeVariantExtensionRequest;
use Illuminate\Http\Request;

class EngineTypeVariantExtController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        return    EngineTypeVariantExt::search($q)->orderBy("created_at",'desc')->paginate($per);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EngineTypeVariantExtensionRequest $request)
    {
        EngineTypeVariantExt::create([
            'label' => $request->input('label'),
            //'engine_type_variant_id' => $request->input('variant_id'),
        ]);


        return response()->json(['status' =>true],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EngineTypeVariantExtensionRequest $request, $id)
    {
        EngineTypeVariantExt::whereId($id)->update([
            'label' => $request->input('label'),
           // 'engine_type_variant_id' => $request->input('variant_id'),
        ]);

        return response()->json(['status' =>true],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EngineTypeVariantExt::whereId($id)->first()->delete();

    }
}

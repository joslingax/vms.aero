<?php

namespace App\Http\Controllers\API;

use App\Engine;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EngineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        return    Engine::with(['manufacturer'])->search($q)->orderBy("engines.created_at",'desc')->paginate($per);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'type' => 'required|unique:engines,type',
            'manufacturer_id' => 'required|exists:engine_manufacturers,id',
        ]);

        $engine = Engine::create([
            'type' => $request->input('type'),
            'manufacturer_id' => $request->input('manufacturer_id'),
        ]);

        return response()->json(['status' =>true],201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'type' => 'required|unique:engines,type,'.$id,
            'manufacturer_id' => 'required|exists:engine_manufacturers,id',
        ]);

      Engine::whereId($id)->update([
            'type' => $request->input('type'),
            'manufacturer_id' => $request->input('manufacturer_id'),
        ]);

        return response()->json(['status' =>true],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Engine::whereId($id)->first()->delete();

    }
}

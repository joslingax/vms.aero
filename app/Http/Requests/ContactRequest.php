<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' =>"required|exists:companies,id",
            'name' =>"required",
            'phones.*.content' => "required",
            'phones.*.is_main' => "required",
            'position' => "required",
            'title' => "required|in:Mrs,Miss,Mr,Dr.,Pr.",
            'phones.*.content' => "nullable",
            'phones.*.is_main' => "nullable",
            'emails.*.content' => "nullable|email|unique:emails,content".($this->method()=="PATCH" ? ",".$this->input('id'):""),
            'emails.*.is_main' => "nullable",
            'faxes.*.content' => "nullable",
            'faxes.*.is_main' => "nullable",
        ];
    }

    public function messages()
    {
        return [

            'company_id.required' =>"The company is required",
            'company_id.exists' =>"This company does not exist",
            'name.required' =>"The name of the contact is required",

            'phones.*.content.required' => "The phone number is required",
            'phones.*.is_main.required' => "You must set a main phone number",

            'emails.*.content.required' => "The email is required",
            'emails.*.content.required' => "This email is already occupied",
            'emails.*.is_main.required' => "You must set a main phone number",

            'faxes.*.content.required' => "The fax is required",
            'faxes.*.is_main.required' => "You must set a main faxnumber",

            'cel.required'=>"The cellphone number is required",
            'tel.required'=>"The Linephone number is required",
            'title.required'=>"The title is required",
            'title.in'=>"This title is not authorized",
            'position.required'=>"The position is required",
            'email.email'=>"The email must have a valid format",
            'email.required'=>"The email is required",

        ];
    }
}

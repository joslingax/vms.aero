<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FleetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       // dd($this->id);
        return [
            
            'serial_number' =>[ "nullable",$this->method()=="POST" ? "unique:fleets,serial_number" : Rule::unique('fleets')->ignore($this->id)],
            'registration' => "",
            'year_of_manufacture' => "nullable|numeric",
            'configuration' => "nullable",
            
            'company_id' =>"required|exists:companies,id",

            'engine_id' =>"nullable|exists:engines,id",
            'engine_type_variant_id' =>"nullable|exists:engine_type_variants,id",
            'engine_type_variant_ext_id' =>"nullable|exists:engine_type_variant_exts,id",

            'aircraft_id' =>"required|exists:aircrafts,id",
            'aircraft_type_variant_id' =>"nullable|exists:aircraft_type_variants,id",
            'aircraft_type_variant_ext_id' =>"nullable|exists:aircraft_type_variant_exts,id",
        ];
    }

    public function messages()
    {
        return [

            'company_id.required' =>"The company is required",
            'company_id.exists' =>"This company does not exist",

            'aircraft_id.required' =>"The aircraft is required",
            'aircraft_id.exists' =>"This aircraft does not exist",

            'aircraft_type_variant_id.required' =>"The aircraft's variant is required",
            'aircraft_type_variant_id.exists' =>"This aircraft's variant does not exist",

            'aircraft_type_variant_ext_id.required' =>"The aircraft's extension is required",
            'aircraft_type_variant_ext_id.exists' =>"This aircraft's extension does not exist",

            'engine_id.required' =>"The engine is required",
            'engine_id.exists' =>"This engine does not exist",

            'engine_type_variant_id.required' =>"The engine's variant is required",
            'engine_type_variant_id.exists' =>"This engine's variant does not exist",

            'engine_type_variant_ext_id.required' =>"The engine's extension is required",
            'engine_type_variant_ext_id.exists' =>"This engine's extension does not exist",



            'serial_number.required' =>"The serial number is required",
            'registration.required'=>"The registration is required",
            'year_of_manufacture.required'=>"The year of manufacture is required",
            'year_of_manufacture.number'=>"The year of manufacture must be a number",
            'year_of_manufacture.min'=>"The year of manufacture must have 4 characters",
            'year_of_manufacture.max'=>"The year of manufacture must have 4 characters",


        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EngineManufacturerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' =>"required|exists:countries,id",
            'name' =>"required|unique:engine_manufacturers,name".($this->method()=="PATCH" ? ",".$this->input('id'):""),
        ];
    }

    public function messages()
    {
        return [

            'country_id.required' =>"The country is required",
            'country_id.exists' =>"This country does not exist",

            'name.required' =>"The name of the manufacturer is required",
            'name.unique' =>"The name of the manufacturer must be unique",


        ];
    }
}

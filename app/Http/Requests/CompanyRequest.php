<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' =>"required|exists:countries,id",
            'address' =>"required",
            'website' => "required",
            'name' => "required",
            'is_airline' => "sometimes|boolean",
            'is_low_cost' => "sometimes|boolean",
            'is_acmi_charter' => "sometimes|boolean",
        ];
    }

    public function messages()
    {
        return [

            'country_id.required' =>"The country is required",
            'country_id.exists' =>"This country does not exist",

            'name.required' =>"The name of the company is required",
            'address.required'=>"The address is required",
            'website.required'=>"The website URL  is required",

            'is_airline.required'=>"Specify if it is a airline company ",
            'is_low_cost.required'=>"Specify if it is a low cost company",
            'is_acmi_charter.required'=>"Specify if it is a acmi charter company",

        ];
    }
}

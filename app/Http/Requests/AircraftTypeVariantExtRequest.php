<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AircraftTypeVariantExtRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'manufacturer_id' =>"required|exists:aircraft_manufacturers,id",
            //'aircraft_id' =>"required|exists:aircrafts,id",
           // 'variant_id' =>"required|exists:aircraft_type_variants,id",
            'label' =>"required|unique:aircraft_type_variant_exts,label".($this->method()=="PATCH" ? ",".$this->input('id'):""),
        ];
    }

    
    public function messages()
    {
        return [

            'aircraft_id.required' =>"The aircraft is required",
            'aircraft_id.exists' =>"This aircraft does not exist",

            'variant_id.required' =>"The variant is required",
            'variant_id.exists' =>"This variant does not exist",

            'manufacturer_id.required' =>"The manufacturer is required",
            'manufacturer_id.exists' =>"This manufacturer does not exist",

            'label.required' =>"The label of the extension is required",
            'label.unique' =>"The label of the extension must be unique",

        ];
    }
}

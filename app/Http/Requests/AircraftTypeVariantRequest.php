<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AircraftTypeVariantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'aircraft_manufacturer_id' =>"required|exists:aircraft_manufacturers,id",
            //'aircraft_id' =>"required|exists:aircrafts,id",
            'label' =>"required|unique:aircraft_type_variants,label".($this->method()=="PATCH" ? ",".$this->input('id'):""),
        ];
    }
}

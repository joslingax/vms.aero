<?php

  return
   [
    "engines"=>
    [
      ["CFMI","CFM56","'United States of America"],
      ["IAE","V2500","'United States of America"],
      ["Pratt & Whitney","PW4056","'United States of America"],
      ["Pratt & Whitney","PW4060","'United States of America"],
      ["Pratt & Whitney","PW4168","'United States of America"],
      ["Pratt & Whitney","PW118","'United States of America"],
      ["Pratt & Whitney","PW119","'United States of America"],
      ["Pratt & Whitney","PW121","'United States of America"],
      ["Pratt & Whitney","PW123","'United States of America"],
      ["Pratt & Whitney","PW124","'United States of America"],
      ["Pratt & Whitney","PW127","'United States of America"],
      ["Pratt & Whitney","PW150","'United States of America"],
      ["Pratt & Whitney","JT9D","'United States of America"],
      ["Pratt & Whitney","JT8D","'United States of America"],
      ["General Electric","CF6","'United States of America"],
      ["General Electric","CF34","'United States of America"],
      ["General Electric","GE90","'United States of America"],
      ["Rolls Royce","RB211","United Kingdom"],
      ["Honeywell","TFE731","'United States of America"],
      
    ]
    ,
    "variants"=>[
      "-2",
      "-3",
      "-5",
      "-7",
      "-50",
      "-80"
    ],
    "extensions"=>[
      "B1",
      "B2",
      "C1",
      "A1",
      "A3",
      "B4",
      "B6",
      "B24",
      "B22",
      "B26",
      "C4",
      "B27",

    ]
   ];